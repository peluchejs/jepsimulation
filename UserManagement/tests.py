"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase
from django.test.client import Client

class SignupTests(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_simple_signup(self):
        response = self.client.post("/users/createuser/", {"first_name": "Jon",
                                                           "last_name": "Sheller",
                                                           "username": "jsheller",
                                                           "password1": "pass",
                                                           "password2": "pass"})
        self.assertEqual(response.status_code, 200)
        
    
    def test_unmatched_password(self):
        pass
    
    