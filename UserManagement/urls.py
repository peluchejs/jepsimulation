from django.conf.urls import patterns, url
from django.contrib.auth.views import logout

urlpatterns = patterns('UserManagement.views',
    url(r'^logout/$', logout),
    url(r'^login/$', 'login_or_create'),
)
