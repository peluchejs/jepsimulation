from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.contrib.auth import login, authenticate
from forms import CustomUserCreationForm, JQUIErrorList
from django.contrib.auth.forms import AuthenticationForm
from Simulator.views import prepare_business

def login_or_create(request):
    if request.method == 'POST': # If the form has been submitted...
        create_form = CustomUserCreationForm(data = request.POST, prefix="create", error_class = JQUIErrorList) # A form bound to the POST data
        login_form = AuthenticationForm(data = request.POST, prefix="login", error_class = JQUIErrorList)
        if create_form.is_valid() or login_form.is_valid(): # All validation rules pass
            if create_form.is_valid():
                create_form.save()
                username = create_form.cleaned_data['username']
                password = create_form.cleaned_data['password2']
                authenticated_user = authenticate(username=username, password=password)
            else:
                authenticated_user = login_form.user_cache
            if authenticated_user:
                login(request, authenticated_user)
                return HttpResponseRedirect(reverse(prepare_business)) # Redirect after POST
            else:
                return HttpResponseRedirect(reverse(login_or_create))
    else:
        create_form = CustomUserCreationForm(prefix="create") # An unbound form
        login_form = AuthenticationForm(prefix="login")

    return render(request, 'login_and_create.html', {
        'create_form': create_form,
        'login_form': login_form,
    })
