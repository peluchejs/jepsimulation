import asyncore, asynchat
import socket, string
import thread
import time
from urllib2 import urlopen

PORT = 6712

patterns = {
}

def sendThread(dest):
	## Huge hack. Instead of firing events in the right spot, just sleep a while
	## Who do you think you are, a tester?
	time.sleep(7)
	urlopen(dest, None, 3)

def headers200(channel):
	channel.push("HTTP/1.0 200 OK\r\n")
	channel.push("Content-type: text/html\r\n")
	channel.push("\r\n")

class HandlerCollection(object):
	def __init__(self, headers, body):
		self.headers = headers
		self.body = body
	
	def callHeaders(self, channel):
		self.headers(channel)
	
	def callBody(self, channel, **kwargs):
		self.body(channel)

class CometEvent(object):
	def __init__(self, waitUrl, triggerUrl):
		global patterns
		self.eventInProgress = False
		self.eventWaiters = []
		self.triggerUrl = triggerUrl
		patterns[waitUrl] = HandlerCollection(headers200, lambda c: self.handleWait(c))
		patterns[triggerUrl] = HandlerCollection(headers200, lambda c: self.handleTrigger(c))

	def handleWait(self, channel):
		if not self.eventInProgress:
			self.eventWaiters.append(channel)
		else:
			channel.push("Event triggered")
			channel.close_when_done()
	
	def handleTrigger(self, channel):
		if not self.eventInProgress:
			self.eventInProgress = True
			for waiter in self.eventWaiters:
				self.handleWait(waiter)
			self.eventInProgress = False
			self.eventWaiters = []
		channel.push("OK")
		channel.close_when_done()
	
	def trigger(self):
		thread.start_new_thread(sendThread, ("http://localhost:" + str(PORT) + "/" + self.triggerUrl,))

enabledEvent = CometEvent("isEnabled", "enable")
disabledEvent = CometEvent("isDisabled", "disable")
refreshEvent = CometEvent("waitForRefresh", "refresh")
newBusinessEvent = CometEvent("waitForNewBusiness", "newBusiness")
mapEvent = CometEvent("refreshMap", "triggerMapRefresh")

def headers404(channel):
	channel.push("HTTP/1.0 404 Not Found\r\n")
	channel.push("Content-type: text/html\r\n")
	channel.push("\r\n")

def error404(channel):
	channel.push("404")
	channel.close_when_done()

errorHandler = HandlerCollection(headers404, error404)

class HTTPChannel(asynchat.async_chat):
	def __init__(self, server, sock, addr):
		asynchat.async_chat.__init__(self, sock)
		self.set_terminator("\r\n")
		self.request = None
		self.data = ""
		self.shutdown = 0

	def collect_incoming_data(self, data):
		self.data = self.data + data

	def found_terminator(self):
		if not self.request:
			# got the request line
			self.request = string.split(self.data, None, 2)
			if len(self.request) != 3:
				self.shutdown = True
			else:
				self.request[1] = self.request[1].lstrip("/")
				self.handler = patterns.get(self.request[1], errorHandler)
				self.handler.callHeaders(self)
			self.data = self.data + "\r\n"
			self.set_terminator("\r\n\r\n") # look for end of headers
		else:
			# return payload.
			# or decide to wait
			self.handler.callBody(self)

class HTTPServer(asyncore.dispatcher):

	def __init__(self, port):
		asyncore.dispatcher.__init__(self)
		self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
		self.bind(("", port))
		self.listen(5)

	def handle_accept(self):
		conn, addr = self.accept()
		HTTPChannel(self, conn, addr)

def runServer():
	hs = HTTPServer(PORT)
	asyncore.loop()

def sendEnabled():
	enabledEvent.trigger()

def sendDisabled():
	disabledEvent.trigger()

def sendRefreshStatus():
	refreshEvent.trigger()

def sendNewBusiness():
	newBusinessEvent.trigger()

def sendRefreshMap():
	mapEvent.trigger()

if __name__ == "__main__":
	runServer()