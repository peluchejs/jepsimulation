from Simulator.signals import simulation_prestep, projector_refresh, simulation_end_step, simulation_new_business
from django.dispatch import receiver
import async_server

## Listen for signals from django to switch between enabled and disabled states
@receiver(simulation_prestep)
def prestepCallback(sender, **kwargs):
    async_server.sendEnabled()

@receiver(simulation_end_step)
def finishedStepCallback(sender, **kwargs):
    async_server.sendRefreshStatus()
    async_server.sendDisabled()

@receiver(simulation_new_business)
def newBusinessCallback(sender, **kwargs):
    async_server.sendNewBusiness()
    async_server.sendRefreshMap()
    
@receiver(projector_refresh)
def projectorCallback(sender, **kwargs):
    async_server.sendRefreshStatus()