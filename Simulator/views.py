from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseBadRequest
from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.views.generic.create_update import update_object, create_object
from django.utils import simplejson

from django.utils.translation import ugettext as _

from forms import ConfigureBusinessForm, ModifyBusinessForm, CreateSimulationForm, getSettingsForm, OverallSettingsForm
from models import Simulation, SimulationStatus, FoodSettings, MechanicSettings, AccountingSettings, CostSettings, PerAgentSettings, ProjectorState
from decorators import admin_required, business_required, simulation_required
from StepTimer.Timer import ServerTimer

## Gameboard load via ajax
@simulation_required()
def gameboard(request):
    if not request.is_ajax():
        return HttpResponseRedirect("/")
    return render(request, "parts/gameboard.html", {"businesses": request.simulation.sorted_businesses})

## Choose which simulation
def choose_simulation(request):
    if "sim_id" in request.GET:
        if request.GET["sim_id"].isdigit():
            sim_id = int(request.GET["sim_id"])
            if Simulation.objects.filter(id = sim_id).exists():
                sim = Simulation.objects.get(id = sim_id)
                if sim.is_active:
                    request.session["simulation-id"] = sim_id
                    if request.user.is_authenticated():
                        if request.user.is_staff:
                            return HttpResponseRedirect(reverse({SimulationStatus.SETUP: configure_panel,
                                                                 SimulationStatus.ADDING_BUSINESSES: wait_for_businesses}.get(sim.status, run_panel)))
                        elif sim.status > SimulationStatus.SETUP:
                            return HttpResponseRedirect(reverse(prepare_business))
                    else:
                        return HttpResponseRedirect(reverse(projector))
        elif request.user.is_authenticated() and request.user.is_staff and request.GET["sim_id"] == "new":
            if "simulation-id" in request.session:
                del request.session["simulation-id"]
            return HttpResponseRedirect(reverse(create_new_sim))
    sims = Simulation.objects.all() if request.user.is_authenticated() and request.user.is_staff else Simulation.objects.filter(status__gt = SimulationStatus.SETUP).filter(is_active = True).all()
    return render(request, "pick_simulation.html", {"simulations": sims, "new_allowed": request.user.is_authenticated() and request.user.is_staff})

## Player functions
@login_required
@simulation_required(redirect = reverse_lazy(choose_simulation))
def prepare_business(request):
    if request.user.is_staff:
        return HttpResponseRedirect(reverse(choose_simulation))
    try:
        b = request.user.business_set.get(simulation__id = request.simulation.id)
        if b is not None:
            return HttpResponseRedirect(reverse(modify_business))
    except ObjectDoesNotExist:
        pass
    if request.method == 'POST':
        form = ConfigureBusinessForm(request.POST, simulation = request.simulation)
        form.instance.simulation = request.simulation
        if form.is_valid():
            business = form.save(commit = False)
            business.user = request.user
            request.simulation.add_business(business)
            n = form.cleaned_data["location_index"]
            if 0 <= n < 4:
                if n % 2 == 0 and business.location_x > 0.5:
                    business.location_x = 1.0 - business.location_x
                if n % 2 == 1 and business.location_x < 0.5:
                    business.location_x = 1.0 - business.location_x
                if n < 2 and business.location_y > 0.5:
                    business.location_y = 1.0 - business.location_y
                if n > 1 and business.location_y < 0.5:
                    business.location_y = 1.0 - business.location_y
            business.save()
            return HttpResponseRedirect(reverse(modify_business))
    else:
        form = ConfigureBusinessForm(simulation = request.simulation)
    return render(request, 'player/prepare_business.html', {
        'form': form,
        'cost_settings': request.simulation.settings.cost_settings,
    })

@login_required
@business_required(redirect = reverse_lazy(prepare_business))
def player_status(request):
    if request.business.can_be_modified:
        return HttpResponseRedirect(reverse(modify_business))
    return render(request, "player/details.html", {"business": request.business,
                                                   "businesses": request.simulation.business_set.all(),
                                                   "simulation": request.simulation})

@login_required
@business_required(redirect = reverse_lazy(prepare_business))
def modify_business(request):
    if request.user.is_staff:
        if not request.is_ajax():
            return HttpResponseRedirect(reverse(choose_simulation))
        else:
            return HttpResponse();
    business = request.business
    if not business.can_be_modified and not request.is_ajax():
        return HttpResponseRedirect(reverse(player_status))
    template_name = "player/parts/modify.html" if request.is_ajax() else "player/modifybusiness.html"
    return update_object(request,
                         form_class = ModifyBusinessForm,
                         object_id = business.current_business_state.id,
                         template_name = template_name,
                         post_save_redirect = reverse(modify_done),
                         extra_context = {"cost_settings": business.simulation.settings.cost_settings,
                                          "business_state": business.current_business_state})    

@login_required
@business_required(redirect = reverse_lazy(prepare_business))
def modify_done(request):
    if not request.business.can_be_modified:
        return HttpResponseRedirect(reverse(player_status))
    return render(request, "player/modify_done.html", {})

@login_required
@business_required(redirect = reverse_lazy(prepare_business))
def view_competition(request):
    template = "player/parts/competition.html" if request.is_ajax() else "player/competition.html"
    return render(request, template, {"business": request.user.business})

@login_required
@business_required(redirect = reverse_lazy(prepare_business))
def view_costs(request):
    template = "player/parts/costs.html" if request.is_ajax() else "player/costs.html"
    return render(request, template, {"business": request.user.business})

@login_required
@business_required(redirect = reverse_lazy(prepare_business))
def view_sales(request):
    template = "player/parts/sales.html" if request.is_ajax() else "player/sales.html"
    return render(request, template, {"business": request.user.business})

## Projector functions
@simulation_required()
def projector(request):
    if request.simulation.projector_state == ProjectorState.GREETING:
        return render(request, "projector/greeting.html")
    elif request.simulation.projector_state == ProjectorState.TIMER:
        return render(request, "projector/timer.html", {"time_remaining": ServerTimer.remaining()})
    elif request.simulation.projector_state == ProjectorState.GAME_BOARD:
        return render(request, "projector/status.html", {"businesses": request.simulation.business_set.all(), "simulation": request.simulation})
    elif request.simulation.projector_state == ProjectorState.USER_DATA:
        return render(request, "projector/details.html", {"business": request.simulation.projector_business})
    elif request.simulation.projector_state == ProjectorState.RUNNING:
        return render(request, "projector/running.html")
    elif request.simulation.projector_state == ProjectorState.MODIFY:
        return render(request, "projector/modify.html")

@simulation_required()
def get_map_data(request):
    neighborhoodData = []
    for i in xrange(4):
        neighborhoodData.append({
            "moto": request.simulation.moto_by_neighborhood[i],
            "food": request.simulation.food_by_neighborhood[i],
            "acct": request.simulation.acct_by_neighborhood[i]})
    json_str = simplejson.dumps(neighborhoodData)
    return HttpResponse(json_str)

@simulation_required()
def get_historical_data(request):
    business = None
    if request.user.is_authenticated():
        if request.user.is_staff:
            return HttpResponseRedirect(reverse(run_panel))
        business = request.user.business_set.get(simulation_id = request.simulation.id)
    elif request.simulation.projector_state == ProjectorState.USER_DATA:
        business = request.simulation.projector_business
    if business is None:
        raise PermissionDenied
    
    states = list(business.states.order_by("roundNumber").all())
    dictstates = map(lambda s: {"roundNumber": s.roundNumber,
                                "income": s.income,
                                "costs": s.costs,
                                "resources": s.resources,
                                "employees": s.employees,
                                "marketing": s.marketing,
                                "price": s.price,
                                "product_quality": s.product_quality,
                                "successful_purchases": s.successful_purchases,
                                "attempted_purchases": s.attempted_purchases}, states)
    json_str = simplejson.dumps({"business_name": business.name, "states": dictstates})
    return HttpResponse(json_str)

def runnextstep(sim):
    if sim.status == SimulationStatus.DURING_STEP:
        sim.set_projector_state(ProjectorState.RUNNING)
        sim.run_step()
        sim.set_projector_state(ProjectorState.GAME_BOARD)
    else:
        if sim.status == SimulationStatus.READY:
            sim.set_projector_state(ProjectorState.MODIFY)
            sim.begin_step()
        else:
            sim.set_projector_state(ProjectorState.GAME_BOARD)

## Admin functions
@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def change_projector_mode(request):
    if "mode" not in request.POST:
        return HttpResponseBadRequest()
    options = {"greeting": ProjectorState.GREETING,
               "user": ProjectorState.USER_DATA,
               "timer": ProjectorState.TIMER,
               "board": ProjectorState.GAME_BOARD,}
    if request.POST["mode"] not in options:
        return HttpResponseBadRequest()
    mode = options[request.POST["mode"]]
    if mode == ProjectorState.USER_DATA:
        if "business_id" not in request.POST or not request.simulation.business_set.filter(id = int(request.POST["business_id"])).exists():
            return HttpResponseBadRequest()
        business = request.simulation.business_set.get(id = int(request.POST["business_id"]))
        request.simulation.set_projector_state(mode, business)
        return HttpResponse("OK")
    elif mode == ProjectorState.TIMER:
        if "seconds" not in request.POST:
            return HttpResponseBadRequest()
        ServerTimer.start(int(request.POST["seconds"]), lambda: runnextstep(request.simulation))
    request.simulation.set_projector_state(mode)
    return HttpResponse("OK")

@login_required
@admin_required(redirect = reverse_lazy(player_status))
def create_new_sim(request):
    return create_object(request,
                         model = Simulation,
                         template_name = "admin/new_sim.html",
                         post_save_redirect = reverse(configure_panel),
                         form_class = CreateSimulationForm)

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def configure_panel(request):
    return render(request, "admin/prepare_sim.html", {"simulation": request.simulation})

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def configuration_complete(request):
    request.simulation.allow_businesses()
    return HttpResponseRedirect(reverse(wait_for_businesses))

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def wait_for_businesses(request):
    return render(request, "admin/wait_for_businesses.html")

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def run_panel(request):
    return render(request, "admin/run_sim.html", {"simulation": request.simulation,
                                                  "businesses": request.simulation.business_set.all()})

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def run_step(request):
    request.simulation.run_step();
    if request.is_ajax():
        return HttpResponse()
    if 'next' in request.GET:
        return HttpResponseRedirect(request.GET['next'])
    return HttpResponseRedirect(reverse(run_panel))

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def begin_step(request):
    request.simulation.begin_step()
    if request.is_ajax():
        return HttpResponse()
    if 'next' in request.GET:
        return HttpResponseRedirect(request.GET['next'])
    return HttpResponseRedirect(reverse(run_panel))

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def begin_sim(request):
    request.simulation.start_simulation()
    return HttpResponseRedirect(reverse(run_panel))

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def get_settings_form(request, formtype):
    sim_settings = request.simulation.settings
    form, inst = {
            "food": (getSettingsForm(FoodSettings), sim_settings.food_settings,),
            "mech": (getSettingsForm(MechanicSettings), sim_settings.mech_settings,),
            "acct": (getSettingsForm(AccountingSettings), sim_settings.acct_settings,),
            "cost": (getSettingsForm(CostSettings), sim_settings.cost_settings,),
            "agent": (getSettingsForm(PerAgentSettings), sim_settings.agent_settings,),
            "overall": (OverallSettingsForm, sim_settings),
        }[formtype]
    
    template = "admin/parts/settings_form.html" if request.is_ajax() else "admin/settings_page.html"
    return update_object(request,
                         form_class = form,
                         object_id = inst.id,
                         template_name = template,
                         post_save_redirect = reverse(configure_panel),
                         extra_context = {"submit_url": request.get_full_path()})

@login_required
@admin_required(redirect = reverse_lazy(player_status))
def admin_warning(request, warning_type):
    warning = _("Once you begin a new simulation you won't be able to use the old simulation. Are you sure you wish to continue?") if warning_type == "new_sim" else _("Once you begin the simulation, you will no longer be able to modify it. Are you sure you wish to continue?")
    continue_url = reverse(create_new_sim) if warning_type == "new_sim" else reverse(begin_sim)
    return render(request, "admin/warning.html", {"warning": warning, "continue_url": continue_url})

@login_required
@admin_required(redirect = reverse_lazy(player_status))
def get_sim_state(request):
    pass

@login_required
@admin_required(redirect = reverse_lazy(player_status))
@simulation_required()
def list_businesses(request):
    return render(request, "admin/parts/business_list.html", {"businesses": request.simulation.business_set.all()})