import django.dispatch

simulation_prestep = django.dispatch.Signal(providing_args = [])
simulation_start_step = django.dispatch.Signal(providing_args = [])
simulation_end_step = django.dispatch.Signal(providing_args = [])
simulation_new_business = django.dispatch.Signal(providing_args = [])
projector_refresh = django.dispatch.Signal(providing_args = [])
