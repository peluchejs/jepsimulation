from django import forms
from models import Business, BusinessState, SimulationSettings, Simulation
from widgets import JQUISlider, JQUISnapSlider, JQUISpinner, LocationPicker

from django.utils.translation import ugettext as _

class ConfigureBusinessForm(forms.ModelForm):
    location_index = forms.IntegerField(min_value = -1, max_value = 3, initial = -1, label = _("Location"))
    class Meta:
        model = Business
        exclude = ("simulation", "user", "can_be_modified", "location_x", "location_y", "state",)
    def __init__(self, *args, **kwargs):
        sim = kwargs.pop("simulation", None)
        super(ConfigureBusinessForm, self).__init__(*args, **kwargs)
        if sim is not None:
            self.fields["size"].widget = JQUISnapSlider(min = sim.settings.min_size, max = sim.settings.max_size)
        
        self.fields["location_index"].widget = LocationPicker()

class CreateSimulationForm(forms.ModelForm):
    class Meta:
        model = Simulation

class ModifyBusinessForm(forms.ModelForm):
    class Meta:
        model = BusinessState
        exclude = ("roundNumber", "resources", "attempted_purchases", "successful_purchases", "business", "total_customer_income", "customer_marketing", "customer_price", "customer_distance", "customer_quality",)
    def __init__(self, *args, **kwargs):
        super(ModifyBusinessForm, self).__init__(*args, **kwargs)
        self.fields["price"].widget = JQUISlider(min = self.instance.business.simulation.settings.price_min,
                                                 max = self.instance.business.simulation.settings.price_max)
        self.fields["marketing"].widget = JQUISlider(min = 0.0, max = 1.0, showText = False, lowLabel = _("None"), highLabel = _("Lots"))
        self.fields["employees"].widget = JQUISpinner(min = self.instance.business.simulation.settings.min_size, max = self.instance.business.size)
        self.fields["product_quality"].widget = JQUISlider(min = 0.0, max = 1.0, showText = False, lowLabel = _("Low"), highLabel = _("High"))

class OverallSettingsForm(forms.ModelForm):
    class Meta:
        model = SimulationSettings
        exclude = ("cost_settings", "food_settings", "mech_settings", "acct_settings", "agent_settings",)

def getSettingsForm(cls):
    class SettingsForm(forms.ModelForm):
        class Meta:
            model = cls
            exclude = ("main_settings",)
    return SettingsForm;