from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from models import Simulation

def simulation_required(**kwargs_outer):
        def decorator(f):
            def satisfy_req_sr(*args, **kwargs):
                redirect = kwargs_outer.get('redirect', '/')
                req = args[0]
                if "simulation-id" in req.session:
                    if Simulation.objects.filter(id=req.session["simulation-id"]).exists():
                        sim = Simulation.objects.get(id=req.session["simulation-id"])
                        if sim.is_active:
                            req.simulation = sim
                            return f(*args, **kwargs)
                return HttpResponseRedirect(redirect)
            return satisfy_req_sr
        return decorator

def business_required(**kwargs_outer):
    def decorator(f):
        @simulation_required(redirect = "/")
        def satisfy_req_br(*args, **kwargs):
            redirect = kwargs_outer.get('redirect', '/')
            req = args[0]
            try:
                b = req.user.business_set.filter(simulation__id = req.simulation.id)
                if not b.exists():
                    return HttpResponseRedirect(redirect)
                req.business = b.get()
                return f(*args, **kwargs)
            except ObjectDoesNotExist:
                return HttpResponseRedirect(redirect)
        return satisfy_req_br
    return decorator

def admin_required(**kwargs_outer):
    def decorator(f):
        def satisfy_req(*args, **kwargs):
            redirect = kwargs_outer.get('redirect', '/')
            request = args[0]
            if not request.user.is_staff:
                return HttpResponseRedirect(redirect)
            return f(*args, **kwargs)
        return satisfy_req
    return decorator