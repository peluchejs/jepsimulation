from django import forms

class JQUISlider(forms.TextInput):
    class Media:
        css = {
               "all": ('css/jquery-ui-1.10.0.custom.min.css',)
               }
        js = ("js/jquery-ui-1.10.0.custom.min.js", "js/fancy_forms.js")

    def __init__(self, **kwargs):
        attrs = kwargs.get("attrs", {})
        classes = attrs.get("class", "")
        attrs["class"] = classes + (" " if classes != "" else "") + "slider"
        attrs["data-min"] = kwargs.pop("min")
        attrs["data-max"] = kwargs.pop("max")
        
        if "showText" in kwargs:
            attrs["data-showText"] = "true" if kwargs.pop("showText") else "false"
        
        if "lowLabel" in kwargs and "highLabel" in kwargs:
            attrs["data-lowLabel"] = kwargs.pop("lowLabel")
            attrs["data-highLabel"] = kwargs.pop("highLabel")
        
        super(JQUISlider, self).__init__(attrs = attrs)

class JQUISnapSlider(forms.TextInput):
    class Media:
        css = {
               "all": ('css/jquery-ui-1.10.0.custom.min.css',)
               }
        js = ("js/jquery-ui-1.10.0.custom.min.js", "js/fancy_forms.js")

    def __init__(self, **kwargs):
        attrs = kwargs.get("attrs", {})
        classes = attrs.get("class", "")
        attrs["class"] = classes + (" " if classes != "" else "") + "snapSlider"
        attrs["data-min"] = kwargs.pop("min")
        attrs["data-max"] = kwargs.pop("max")
        
        if "showText" in kwargs:
            attrs["data-showText"] = kwargs.pop("showText")
            
        super(JQUISnapSlider, self).__init__(attrs = attrs)

class JQUISpinner(forms.TextInput):
    class Media:
        css = {
               "all": ('css/jquery-ui-1.10.0.custom.min.css',)
               }
        js = ("js/jquery-ui-1.10.0.custom.min.js", "js/fancy_forms.js")

    def __init__(self, **kwargs):
        attrs = kwargs.get("attrs", {})
        classes = attrs.get("class", "")
        attrs["class"] = classes + (" " if classes != "" else "") + "spinner"
        attrs["data-min"] = kwargs.pop("min")
        attrs["data-max"] = kwargs.pop("max")
        
        super(JQUISpinner, self).__init__(attrs = attrs)

class LocationPicker(forms.TextInput):
    class Media:
        css = {
               "all": ('css/jquery-ui-1.10.0.custom.min.css', 'css/location.css')
               }
        js = ("js/jquery-ui-1.10.0.custom.min.js", "js/comet.js", "js/location.js", "js/fancy_forms.js")

    def __init__(self, **kwargs):
        attrs = kwargs.get("attrs", {})
        classes = attrs.get("class", "")
        attrs["class"] = classes + (" " if classes != "" else "") + "locationPicker"
            
        super(LocationPicker, self).__init__(attrs = attrs, **kwargs)