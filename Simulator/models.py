from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import post_save

from signals import simulation_prestep, simulation_start_step, simulation_end_step, simulation_new_business, projector_refresh
from exceptions import SimulationStateException, ProjectorOperationException

from math import log10, atan, pi, sqrt, floor
import random

from django.utils.translation import ugettext_lazy as _

def RGBtoint(rgb):
    return ((int(rgb[0] * 255) << 16) & 0xff0000) | ((int(rgb[1] * 255) << 8) & 0x00ff00) | ((int(rgb[2] * 255)) & 0x0000ff)

def HSVtoRGB(hsv):
    h = hsv[0]
    s = hsv[1]
    v = hsv[2]
    hp = h / 60.0
    c = v * s
    x = c * (1.0 - abs(int(hp) % 2 - 1))
    if 0 <= hp < 1:
        rgb = (c,x,0)
    elif 1 <= hp < 2:
        rgb = (x, c, 0)
    elif 2 <= hp < 3:
        rgb = (0, c, x)
    elif 3 <= hp < 4:
        rgb = (0, x, c)
    elif 4 <= hp < 5:
        rgb = (x, 0, c)
    else:
        rgb = (c, 0, x)
    m = v - c
    return (rgb[0] + m, rgb[1] + m, rgb[2] + m)

def clamp(value, minimum, maximum):
    return min(maximum, max(minimum, value))

def getPurchaseProbability(weight):
    return 1.4 * (atan(2.0 * (weight - 0.75)) + pi / 2.0) / pi

def getRestaurantDesire(sim):
    settings = sim.settings.food_settings
    return clamp(random.gauss(settings.desire_avg, settings.desire_dev), 0.0, 1.0)

def getWorkshopDesire(sim):
    settings = sim.settings.mech_settings
    desire = 0.0
    if random.uniform(0,1) > settings.desire_high_odds: 
        desire = random.gauss(settings.desire_avg, settings.desire_dev)
    else:
        desire = random.gauss(settings.desire_high_avg, settings.desire_high_dev)
    return clamp(desire, 0.0, 1.0)

def getAccountingDesire(sim):
    settings = sim.settings.acct_settings
    roundNum = sim.round_num
    desire = 0.0
    if roundNum % settings.desire_high_freq != 0: 
        desire = random.gauss(settings.desire_avg, settings.desire_dev)
    else:
        desire = random.gauss(settings.desire_high_avg, settings.desire_high_dev)
    return clamp(desire, 0.0, 1.0)

class SimulationStatus:
    SETUP = 0
    ADDING_BUSINESSES = 1
    READY = 2
    DURING_STEP = 3
    DEFAULT = SETUP
    CHOICES = (
        (SETUP, _("Setup")),
        (ADDING_BUSINESSES, _("Adding")),
        (READY, _("Ready")),
        (DURING_STEP, _("Stepping")),
    )

class BusinessAreas:
    ACCOUNTING = 0
    RESTAURANT = 1
    WORKSHOP = 2
    DEFAULT = WORKSHOP
    CHOICES = (
        (ACCOUNTING, _("Accounting")),
        (RESTAURANT, _("Restaurant")),
        (WORKSHOP, _("Workshop")),
    )
    DesireDistributions = {
        RESTAURANT: lambda s: getRestaurantDesire(s),
        WORKSHOP: lambda s: getWorkshopDesire(s),
        ACCOUNTING: lambda s: getAccountingDesire(s)
    }

class ProjectorState:
    GREETING = 0
    TIMER = 1
    GAME_BOARD = 2
    USER_DATA = 3
    RUNNING = 4
    MODIFY = 5
    DEFAULT = GREETING
    CHOICES = (
        (GREETING, _("Greeting")),
        (TIMER, _("Timer")),
        (GAME_BOARD, _("Game board")),
        (USER_DATA, _("User information")),
        (RUNNING, _("Running step")),
        (MODIFY, _("Modify businesses")),
    )
    
class FoodSettings(models.Model):
    desire_avg = models.FloatField(default = 0.6, verbose_name = _("Desire average"))
    desire_dev = models.FloatField(default = 0.3, verbose_name = _("Desire deviation"))

class MechanicSettings(models.Model):
    desire_avg = models.FloatField(default = 0.45, verbose_name = _("Desire average"))
    desire_dev = models.FloatField(default = 0.3, verbose_name = _("Desire deviation"))
    desire_high_odds = models.FloatField(default = 0.2, verbose_name = _("Odds of high desire"))
    desire_high_avg = models.FloatField(default = 1.0, verbose_name = _("High desire average"))
    desire_high_dev = models.FloatField(default = 0.2, verbose_name = _("High desire deviation"))

class AccountingSettings(models.Model):
    desire_avg = models.FloatField(default = 0.35, verbose_name = _("Desire average"))
    desire_dev = models.FloatField(default = 0.2, verbose_name = _("Desire deviation"))
    desire_high_freq = models.IntegerField(default = 3, verbose_name = _("Frequency of high desire"))
    desire_high_avg = models.FloatField(default = 0.9, verbose_name = _("High desire average"))
    desire_high_dev = models.FloatField(default = 0.3, verbose_name = _("High desire deviation"))

class PerAgentSettings(models.Model):
    income_max = models.FloatField(default = 7.0, verbose_name = _("Maximum income"))
    income_min = models.FloatField(default = 1.5, verbose_name = _("Minimum income"))
    income_inequality = models.FloatField(default = 0.02, verbose_name = _("Income inequality"))
    persistence_average = models.FloatField(default = 0.5, verbose_name = _("Average persistence"))
    persistence_deviation = models.FloatField(default = 0.2, verbose_name = _("Persistence deviation"))
    price_sense_average = models.FloatField(default = 0.7, verbose_name = _("Average price sensitivity"))
    price_sense_deviation = models.FloatField(default = 0.3)
    distance_sense_average = models.FloatField(default = 0.6)
    distance_sense_deviation = models.FloatField(default = 0.4)
    marketing_sense_average = models.FloatField(default = 0.5)
    marketing_sense_deviation = models.FloatField(default = 0.5)
    quality_sense_average = models.FloatField(default = 0.5)
    quality_sense_deviation = models.FloatField(default = 0.4)
    base_weights_average = models.FloatField(default = 0.6)
    base_weights_deviation = models.FloatField(default = 0.2)
    carryover_average = models.FloatField(default = 0.4)
    carryover_deviation = models.FloatField(default = 0.3)
    random_variance = models.FloatField(default = 0.05)
    
    def incomeDistribution(self, x):
        INCOME_MIN = self.income_min
        INCOME_MAX = self.income_max
        INCOME_INEQUALITY = self.income_inequality
        MAX_FACTOR = 1.0 - pow(INCOME_MAX - INCOME_MIN + 1.0, -INCOME_INEQUALITY)
        return INCOME_MIN + pow(10.0, (-log10(1.0-MAX_FACTOR*x)/INCOME_INEQUALITY))

    def getIncome(self):
        return self.incomeDistribution(random.uniform(0,1))
    
    def getPersistence(self):
        return clamp(random.gauss(self.persistence_average, self.persistence_deviation), 0.0, 1.0)
    
    def getDistanceSensitivity(self):
        return clamp(random.gauss(self.distance_sense_average, self.distance_sense_deviation), 0.0, 1.0)
    
    def getPriceSensitivity(self):
        return clamp(random.gauss(self.price_sense_average, self.price_sense_deviation), 0.0, 1.0)
    
    def getMarketingSensitivity(self):
        return clamp(random.gauss(self.marketing_sense_average, self.marketing_sense_deviation), 0.0, 1.0)

    def getQualitySensitivity(self):
        return clamp(random.gauss(self.quality_sense_average, self.quality_sense_deviation), 0.0, 1.0)
    
    def getBaseWeight(self):
        return clamp(random.gauss(self.base_weights_average, self.base_weights_deviation), 0.1, 0.9)

    def getCarryoverPercentage(self):
        return clamp(random.gauss(self.carryover_average, self.carryover_deviation), 0.0, 1.0)
    
    def getRandomVariance(self):
        return random.uniform(-self.random_variance, self.random_variance)

class CostSettings(models.Model):
    cost_per_employee = models.FloatField(default = 3.0)
    base_rent_cost = models.FloatField(default = 2.0)
    rent_costs = models.FloatField(default = 0.5)
    marketing_cost = models.FloatField(default = 2.0)
    product_cost = models.FloatField(default = 1.0)

class SimulationSettings(models.Model):
    base_agent_count = models.PositiveIntegerField(default = 7)
    per_business_agent_count = models.PositiveIntegerField(default = 10)
    num_purchasing_rounds = models.PositiveIntegerField(default = 3)
    starting_resources = models.FloatField(default = 150.0)
    
    price_max = models.FloatField(default = 5.0)
    price_min = models.FloatField(default = 2.0)
    
    min_size = models.PositiveIntegerField(default = 2)
    max_size = models.PositiveIntegerField(default = 9)
    
    cost_settings = models.OneToOneField(CostSettings, default = lambda:CostSettings.objects.create())
    food_settings = models.OneToOneField(FoodSettings, default = lambda:FoodSettings.objects.create())
    mech_settings = models.OneToOneField(MechanicSettings, default = lambda:MechanicSettings.objects.create())
    acct_settings = models.OneToOneField(AccountingSettings, default = lambda:AccountingSettings.objects.create())
    agent_settings = models.OneToOneField(PerAgentSettings, default = lambda:PerAgentSettings.objects.create())
    
    def normalizePrice(self, price):
        PRICE_MIN = self.price_min
        PRICE_MAX = self.price_max
        return (price - PRICE_MIN) / (PRICE_MAX - PRICE_MIN)
    
    def getAdjustedPriceSensitivity(self, base_sensitivity, resources):
        PRICE_MIN = self.price_min
        PRICE_MAX = self.price_max
        PRICE_MULT_FACT = 1.5 / (PRICE_MIN - PRICE_MAX)
        PRICE_ADD_FACT = 2.0 - PRICE_MULT_FACT * PRICE_MIN
        return base_sensitivity * clamp(PRICE_MULT_FACT * resources + PRICE_ADD_FACT, 0.5, 2.0)
    
    class AreaSettingsImpl(object):
        def __init__(self, settings):
            self._areasettings = {
                BusinessAreas.FOOD: settings.foodsettings,
                BusinessAreas.WORKSHOP: settings.mechanicsettings,
                BusinessAreas.ACCOUNTING: settings.accountingsettings
            }
        def __getitem__(self, index):
            return self._areasettings[index]
    
    @property
    def AreaSettings(self):
        if "areasettings" not in vars(self):
            self.areasettings = self.AreaSettingsImpl(self)
        return self.areasettings
    
    def clean(self):
        from django.core.exceptions import ValidationError
        if self.price_min > self.price_max:
            raise ValidationError("Price range incorrect")
        if self.min_size > self.max_size:
            raise ValidationError("Size range incorrect")

class Simulation(models.Model):
    is_active = models.BooleanField(default = True)
    name = models.CharField(max_length = 25, verbose_name = _("Name"))
    settings = models.OneToOneField(SimulationSettings, editable = False, default = lambda:SimulationSettings.objects.create())
    round_num = models.IntegerField(default = 0, editable = False)
    status = models.IntegerField(choices = SimulationStatus.CHOICES,
                                 default = SimulationStatus.DEFAULT,
                                 editable = False)
    
    projector_state = models.IntegerField(choices = ProjectorState.CHOICES,
                                          default = ProjectorState.DEFAULT)
    projector_business = models.ForeignKey("Business", editable = False, null = True, related_name = "+")
    
    food_desire_pred = models.FloatField(editable = False, default = 0.0)
    acct_desire_pred = models.FloatField(editable = False, default = 0.0)
    mech_desire_pred = models.FloatField(editable = False, default = 0.0)
    
    LOW_DESIRE_STR = _("Low")
    MID_DESIRE_STR = _("Normal")
    HIGH_DESIRE_STR = _("High")
    
    def set_projector_state(self, state, business = None):
        if state == ProjectorState.USER_DATA and business is None:
            raise ProjectorOperationException("Business is required to set user data")
        self.projector_state = state
        if business is not None:
            self.projector_business = business
        self.save()
        projector_refresh.send(self)

    @property
    def sorted_businesses(self):
        return self.business_set.order_by("").reverse()
    
    def get_demand_prediction(self, area):
        desire_level = {BusinessAreas.ACCOUNTING: self.acct_desire_pred,
                        BusinessAreas.RESTAURANT: self.food_desire_pred,
                        BusinessAreas.WORKSHOP: self.mech_desire_pred}[area]
        if desire_level < 1.55:
            return self.LOW_DESIRE_STR
        if desire_level > 1.66:
            return self.HIGH_DESIRE_STR
        return self.MID_DESIRE_STR
    
    def allow_businesses(self):
        lock = Simulation.objects.select_for_update().filter(id = self.id)
        if self.status != SimulationStatus.SETUP:
            raise SimulationStateException("Simulation not in state for allowing new businesses")
        self.status = SimulationStatus.ADDING_BUSINESSES
        self.save()
    
    def add_business(self, business):
        lock = Simulation.objects.select_for_update().filter(id = self.id)
        if self.status != SimulationStatus.ADDING_BUSINESSES:
            raise SimulationStateException("Simulation doesn't allow adding businesses right now.")
        business.simulation = self
    
    def start_simulation(self):
        lock = Simulation.objects.select_for_update().filter(id = self.id)
        if self.status != SimulationStatus.ADDING_BUSINESSES:
            raise SimulationStateException("Simulation must have businesses added before starting")
        self.status = SimulationStatus.DURING_STEP
        self.generateAgents()
        self.moveAgents()
        self.setColors()
        self.generatePrediction(self.agent_set.all())
        self.save()

    def generatePrediction(self, agents):
        self.food_desire_pred = 0.0
        self.acct_desire_pred = 0.0
        self.mech_desire_pred = 0.0
        
        for a in agents:
            round_desire = [BusinessAreas.DesireDistributions[area](self) for area in xrange(len(BusinessAreas.CHOICES))]
            carryover_desire = [a.acct_carryover, a.food_carryover, a.mech_carryover]
            base_weights = [a.acct_base_weight, a.food_base_weight, a.mech_base_weight]
            pred_desire = [sum(x) for x in zip(round_desire, carryover_desire, base_weights)]
            self.acct_desire_pred += pred_desire[0]
            self.food_desire_pred += pred_desire[1]
            self.mech_desire_pred += pred_desire[2]
        
        agent_count = len(agents)
        self.food_desire_pred /= agent_count
        self.acct_desire_pred /= agent_count
        self.mech_desire_pred /= agent_count
    
    def generateAgents(self):
        agent_count = self.settings.base_agent_count + self.settings.per_business_agent_count * self.business_set.count()
        Agent.objects.bulk_create(map(lambda x:Agent.create(simulation = self), xrange(agent_count)))
    
    def getQuartile(self, income):
        settings = self.settings.agent_settings
        percent = (income - settings.income_min) / (settings.income_max - settings.income_min)
        q = int(floor(percent * 3))
        if q < 0:
            q = 0
        elif q > 3:
            q = 3
        return q
    
    class ByNeighborhoodImpl(object):
        def __init__(self, qs):
            self._qs = qs
        def __getitem__(self, index):
            index = int(index)
            if index == 0:
                return self._qs.filter(location_x__lt = 0.5, location_y__lt = 0.5).count()
            if index == 1:
                return self._qs.filter(location_x__gt = 0.5, location_y__lt = 0.5).count()
            if index == 2:
                return self._qs.filter(location_x__lt = 0.5, location_y__gt = 0.5).count()
            if index == 3:
                return self._qs.filter(location_x__gt = 0.5, location_y__gt = 0.5).count()
    
    @property
    def moto_by_neighborhood(self):
        return Simulation.ByNeighborhoodImpl(self.business_set.filter(area = BusinessAreas.WORKSHOP))
    
    @property
    def acct_by_neighborhood(self):
        return Simulation.ByNeighborhoodImpl(self.business_set.filter(area = BusinessAreas.ACCOUNTING))
    
    @property
    def food_by_neighborhood(self):
        return Simulation.ByNeighborhoodImpl(self.business_set.filter(area = BusinessAreas.RESTAURANT))
    
    @property
    def ordered_businesses(self):
        businesses = list(self.business_set.all())
        businesses.sort(key=lambda b:b.current_business_state.resources, reverse=True)
        return businesses
    
    def moveAgents(self):
        agents = Agent.objects.filter(simulation_id = self.id).all()
        for a in agents:
            q = self.getQuartile(a.income + random.uniform(-1.0, 0.5))
            if q == 3:
                if a.location_x > 0.5:
                    a.location_x = 1.0 - a.location_x
                if a.location_y > 0.5:
                    a.location_y = 1.0 - a.location_y
            elif q == 1 or q == 2:
                neighborhood = random.randint(0, 1)
                if neighborhood == 0:
                    if a.location_x < 0.5:
                        a.location_x = 1.0 - a.location_x
                    if a.location_y > 0.5:
                        a.location_y = 1.0 - a.location_y
                else:
                    if a.location_x > 0.5:
                        a.location_x = 1.0 - a.location_x
                    if a.location_y < 0.5:
                        a.location_y = 1.0 - a.location_y
            else:
                if a.location_x < 0.5:
                    a.location_x = 1.0 - a.location_x
                if a.location_y < 0.5:
                    a.location_y = 1.0 - a.location_y
            a.save()
    
    def setColors(self):
        colorCount = self.business_set.count()
        if colorCount == 0:
            return
        mult = 360.0 / colorCount
        for b in self.business_set.all():
            colorCount -= 1
            b.color = RGBtoint(HSVtoRGB((mult * colorCount, 0.6, 0.7)))
            b.save()
    
    def begin_step(self):
        lock = Simulation.objects.select_for_update().filter(id = self.id)
        if self.status != SimulationStatus.READY:
            raise SimulationStateException("Not running")
        for b in self.business_set.all():
            b.startSimulationStep(self.round_num)
            b.save()
        self.status = SimulationStatus.DURING_STEP
        self.save()
        simulation_prestep.send(self)
    
    def run_step(self):
        lock = Simulation.objects.select_for_update().filter(id = self.id)
        if self.status != SimulationStatus.DURING_STEP:
            raise SimulationStateException("Must have begun step before stepping")
        simulation_start_step.send(self)
        businesses = list(self.business_set.all())
        purchasing_agents = list(self.agent_set.all())
        for pa in purchasing_agents:
            pa.startPurchaseRound(self.round_num)
        potential_clients = list(purchasing_agents)
        for i in xrange(self.settings.num_purchasing_rounds):
            for b in businesses:
                b.startPurchaseRound()
            unsatisfied_agents = list()
            random.shuffle(potential_clients)
            for pc in potential_clients:
                b = pc.tryToBuy(businesses)
                if pc.wantToTryAgain():
                    unsatisfied_agents.append(pc)
            potential_clients = unsatisfied_agents
            
        for pa in purchasing_agents:
            pa.endPurchaseRound()
        for b in businesses:
            b.endSimulationStep()
        self.round_num += 1
        self.generatePrediction(purchasing_agents)
        self.status = SimulationStatus.READY
        self.save()
        simulation_end_step.send(self)

class Agent(models.Model):
    persistence = models.FloatField()
    income = models.FloatField()
    food_base_weight = models.FloatField()
    mech_base_weight = models.FloatField()
    acct_base_weight = models.FloatField()
    food_carryover = models.FloatField(default = 0.0)
    mech_carryover = models.FloatField(default = 0.0)
    acct_carryover = models.FloatField(default = 0.0)
    distance_sensitivity = models.FloatField()
    price_sensitivity = models.FloatField()
    marketing_sensitivity = models.FloatField()
    quality_sensitivity = models.FloatField()
    carryover_percentage = models.FloatField()
    resources = models.FloatField(default = 0.0)
    location_x = models.FloatField(default = lambda:random.uniform(0.0, 1.0))
    location_y = models.FloatField(default = lambda:random.uniform(0.0, 1.0))
    simulation = models.ForeignKey(Simulation)
    
    @classmethod
    def create(cls, *args, **kwargs):
        a = Agent(*args, **kwargs)
        if "simulation" in kwargs:
            settings = kwargs["simulation"].settings.agent_settings
            a.income = settings.getIncome()
            a.persistence = settings.getPersistence()
            a.distance_sensitivity = settings.getDistanceSensitivity()
            a.price_sensitivity = settings.getPriceSensitivity()
            a.marketing_sensitivity = settings.getMarketingSensitivity()
            a.quality_sensitivity = settings.getQualitySensitivity()
            a.food_base_weight = settings.getBaseWeight()
            a.mech_base_weight = settings.getBaseWeight()
            a.acct_base_weight = settings.getBaseWeight()
            a.carryover_percentage = settings.getCarryoverPercentage()
        return a
    
    def startPurchaseRound(self, roundNum):
        self.round_desire = [BusinessAreas.DesireDistributions[area](self.simulation) for area in xrange(len(BusinessAreas.CHOICES))]
        self.carryover_desire = [self.acct_carryover, self.food_carryover, self.mech_carryover]
        self.base_weights = [self.acct_base_weight, self.food_base_weight, self.mech_base_weight]
        self.current_desire = [sum(x) for x in zip(self.round_desire, self.carryover_desire, self.base_weights)]
        self.attempted_purchases = []
        self.resources += self.income
        self.adjusted_price_sensitivity = self.simulation.settings.getAdjustedPriceSensitivity(self.price_sensitivity, self.resources)
        
    def tryToBuy(self, businesses):
        b_weights = [self.calcBusinessWeight(b) for b in businesses]
        combined_b_w = zip(b_weights, businesses)
        combined_by_area = [[b_w for b_w in combined_b_w if b_w[1].area == area and b_w[1].price <= self.resources] for area in xrange(3)]
        for arealist in combined_by_area:
            arealist.sort(reverse = True)
        possible_purchases = [x[0] for x in combined_by_area if len(x) > 0]
        possible_purchases.sort(reverse = True)
        if len(possible_purchases) > 0 and random.uniform(0, 1) < getPurchaseProbability(possible_purchases[0][0]):
            if possible_purchases[0][1].tryPurchase(self):
                self.resources -= possible_purchases[0][1].price
                self.current_desire[possible_purchases[0][1].area] = 0.0
                return possible_purchases[0][1]
            else:
                self.attempted_purchases.append(possible_purchases[0][1])
        return None
    def wantToTryAgain(self):
        settings = self.simulation.settings
        return self.resources >= settings.price_min and random.uniform(0, 1) < self.persistence
    def getDistanceFrom(self, business):
        return sqrt(pow(self.location_x - business.location_x, 2.0) + pow(self.location_y - business.location_y, 2.0))
    def calcBusinessWeight(self, business):
        return self.base_weights[business.area] * (self.current_desire[business.area]
                                                    - 2.0 * (business in self.attempted_purchases)
                                                    - self.adjusted_price_sensitivity * self.simulation.settings.normalizePrice(business.price)
                                                    - 2.0 * self.distance_sensitivity * self.getDistanceFrom(business)
                                                    + self.marketing_sensitivity * business.marketing
                                                    + 3.0 * self.quality_sensitivity * business.product_quality
                                                    + self.simulation.settings.agent_settings.getRandomVariance())
    def endPurchaseRound(self):
        self.food_carryover = clamp(self.current_desire[BusinessAreas.RESTAURANT] * self.carryover_percentage, 0.0, 1.0)
        self.mech_carryover = clamp(self.current_desire[BusinessAreas.WORKSHOP] * self.carryover_percentage, 0.0, 1.0)
        self.acct_carryover = clamp(self.current_desire[BusinessAreas.ACCOUNTING] * self.carryover_percentage, 0.0, 1.0)
        self.save()
        

class BusinessState(models.Model):
    roundNumber = models.IntegerField(default = -1, editable = False, verbose_name = _("Round number"))
    resources = models.FloatField(default = 0.0, editable = False, verbose_name = _("Resources"))
    attempted_purchases = models.IntegerField(default = 0, editable = False, verbose_name = _("Attempted purchases"))
    successful_purchases = models.IntegerField(default = 0, editable = False, verbose_name = _("Completed purchases"))
    employees = models.IntegerField(default = 2, verbose_name = _("Employees"))
    marketing = models.FloatField(default = 0.5, verbose_name = _("Marketing"))
    price = models.FloatField(default = 2.0, verbose_name = _("Price"))
    product_quality = models.FloatField(default = 0.5, verbose_name = _("Product quality"))
    total_customer_income = models.FloatField(default = 0.0, editable = False, verbose_name = _("Total customer income"))
    customer_marketing = models.FloatField(default = 0.0, editable = False)
    customer_price = models.FloatField(default = 0.0, editable = False)
    customer_distance = models.FloatField(default = 0.0, editable = False)
    customer_quality = models.FloatField(default = 0.0, editable = False)
    upgrade_costs = models.FloatField(default = 0.0, editable = False)
    business = models.ForeignKey('Business', related_name = "states", editable = False)
    
    def clean(self):
        from django.core.exceptions import ValidationError
        settings = self.business.simulation.settings
        if self.price < settings.price_min or self.price > settings.price_max:
            raise ValidationError("Price out of range")
        if self.employees > self.business.size or self.employees < 1:
            raise ValidationError("Employees out of range")
        if self.marketing < 0.0 or self.marketing > 1.0:
            raise ValidationError("Marketing out of range")
        if self.product_quality < 0.0 or self.product_quality > 1.0:
            raise ValidationError("Quality out of range")
        if self.resources < 0:
            self.resources = 0
    
    @property
    def employee_costs(self):
        settings = self.business.simulation.settings.cost_settings
        return settings.cost_per_employee * self.employees
    
    @property
    def rent_costs(self):
        settings = self.business.simulation.settings.cost_settings
        return settings.base_rent_cost + self.business.size * settings.rent_costs
    
    @property
    def marketing_costs(self):
        settings = self.business.simulation.settings.cost_settings
        return settings.marketing_cost * self.marketing
    
    @property
    def product_costs(self):
        setting = self.business.simulation.settings.cost_settings
        return (setting.product_cost * self.product_quality + 1.0) * self.successful_purchases
    
    @property
    def costs(self):
        return self.fixed_costs + self.variable_costs + self.one_time_costs
    
    @property
    def fixed_costs(self):
        return self.employee_costs + self.rent_costs + self.marketing_costs
    
    @property
    def variable_costs(self):
        return self.product_costs
    
    @property
    def one_time_costs(self):
        return self.upgrade_costs
    
    @property
    def income(self):
        return self.successful_purchases * self.price
    
    @property
    def net_profit(self):
        return self.income - self.costs
    
    @property
    def abs_net_profit(self):
        return abs(self.net_profit)
    
    @property
    def idle_employees(self):
        settings = self.business.simulation.settings
        return settings.num_purchasing_rounds * self.employees - self.successful_purchases
    
    @property
    def avg_customer_income(self):
        if self.successful_purchases == 0:
            return 0.0
        return self.total_customer_income / self.successful_purchases
    
    @property
    def lost_sales(self):
        return self.attempted_purchases - self.successful_purchases
    
    def calculate_badges(self):
        settings = self.business.simulation.settings
        if (float(self.successful_purchases) / self.attempted_purchases) < 0.33:
            b = Badge.objects.create(business_state = self,
                                     title = BadgeStrings.NotEnoughEmployeesTitle,
                                     description = BadgeStrings.NotEnoughEmployeesDescription)
            b.save()
        if self.idle_employees > (settings.num_purchasing_rounds * self.employees) * 0.6:
            b = Badge.objects.create(business_state = self,
                                     title = BadgeStrings.IdleEmployeesTitle,
                                     description = BadgeStrings.IdleEmployeesDescription)
            b.save()
        if self.successful_purchases < 4:
            b = Badge.objects.create(business_state = self,
                                     title = BadgeStrings.NoSalesTitle,
                                     description = BadgeStrings.NoSalesDescription)
            b.save()
        if self.marketing < 0.3 and (
            self.customer_marketing < self.customer_distance and
            self.customer_marketing < self.customer_price and
            self.customer_marketing < self.customer_quality):
            b = Badge.objects.create(business_state = self,
                                     title = BadgeStrings.LowMarketingTitle,
                                     description = BadgeStrings.LowMarketingDescription)
            b.save()
        if self.marketing > 0.7 and (
           self.customer_marketing > self.customer_distance and
            self.customer_marketing > self.customer_price and
            self.customer_marketing > self.customer_quality):
            b = Badge.objects.create(business_state = self,
                                     title = BadgeStrings.HighMarketingTitle,
                                     description = BadgeStrings.HighMarketingDescription)
            b.save()
        if self.net_profit < 0 and self.successful_purchases > self.employees * 2:
            b = Badge.objects.create(business_state = self,
                                     title = BadgeStrings.HighCostsTitle,
                                     description = BadgeStrings.HighCostsDescription)
            b.save()

class BadgeStrings(object):
    NotEnoughEmployeesTitle = _("Slow Service")
    NotEnoughEmployeesDescription = _("Many customers were turned away because there were not enough employees to help them.")
    IdleEmployeesTitle = _("Work? What work?")
    IdleEmployeesDescription = _("Your employees didn't have enough work to do, so they just sat around, costing you money.")
    NoSalesTitle = _("Is that place open?")
    NoSalesDescription = _("You had few (or no!) sales.")
    HighCostsTitle = _("The Red (Ink) Sea")
    HighCostsDescription = _("There were plenty of sales, but costs were so high that you still couldn't make any money.")
    LowMarketingTitle = _("Who are you?")
    LowMarketingDescription = _("Your marketing may be too low for the community to know about you")
    HighMarketingTitle = _("Smooth Talker")
    HighMarketingDescription = _("Customers loved your marketing, even though your product is the same as everyone else.")
    GoodValueTitle = _("Margins like a razor blade")
    GoodValueDescription = _("Your business had the lowest profit margins of all. Great for the customer, but did you make any money?")
    LowValueTitle = _("There's one born every minute")
    LowValueDescription = _("You provided the worst product for its price. Great margins, but those customers must be real suckers. Got a bridge I can buy?")

class Badge(models.Model):
    business_state = models.ForeignKey(BusinessState)
    title = models.CharField(max_length = 40)
    description = models.TextField()
    
class Business(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length = 50, verbose_name = _("Name"))
    location_x = models.FloatField(default = lambda:random.uniform(0.20, 0.8))
    location_y = models.FloatField(default = lambda:random.uniform(0.20, 0.8))
    area = models.IntegerField(choices = BusinessAreas.CHOICES,
                               default = BusinessAreas.DEFAULT,
                               verbose_name = _("Area"))
    size = models.IntegerField(default = 6, verbose_name = _("Size"))
    color = models.IntegerField(editable = False, null = True)
    simulation = models.ForeignKey(Simulation, editable = False)
    
    def clean(self):
        from django.core.exceptions import ValidationError
        settings = self.simulation.settings
        if self.size < settings.min_size or self.size > settings.max_size:
            raise ValidationError("Size out of range")
    
    @property
    def current_business_state(self):
        if "state" not in vars(self):
            if self.states.exists():
                self.state = self.states.order_by("-roundNumber")[0]
            else:
                self.state = BusinessState.objects.create(roundNumber = 0, business = self, resources = self.simulation.settings.starting_resources)
                self.state.save()
        return self.state
    
    @property
    def can_be_modified(self):
        return self.simulation.status == SimulationStatus.ADDING_BUSINESSES or self.simulation.status == SimulationStatus.DURING_STEP
    
    @property
    def get_demand_prediction(self):
        return self.simulation.get_demand_prediction(self.area)
    
    @property
    def ordered_states(self):
        states = list(self.states.order_by("-roundNumber")[:5])
        states.reverse()
        return states
    
    @property
    def price(self):
        return self.current_business_state.price
    
    @property
    def marketing(self):
        return self.current_business_state.marketing
    
    @property
    def employees(self):
        return self.current_business_state.employees
    
    @property
    def product_quality(self):
        return self.current_business_state.product_quality
    
    @property
    def nearby_competition(self):
        comp = self.simulation.business_set
        if self.location_x > 0.5:
            comp = comp.filter(location_x__gt = 0.5)
        else:
            comp = comp.filter(location_x__lte = 0.5)
        if self.location_y > 0.5:
            comp = comp.filter(location_y__gt = 0.5)
        else:
            comp = comp.filter(location_y__lte = 0.5)
        return comp
    
    def startSimulationStep(self, roundNum):
        prevState = None
        if self.states.count() > 0:
            prevState = self.current_business_state
        st = BusinessState.objects.create(business = self)
        st.attempted_purchases = 0
        st.successful_purchases = 0
        if prevState is not None:
            st.employees = prevState.employees
            st.price = prevState.price
            st.marketing = prevState.marketing
            st.resources = prevState.resources
            st.product_quality = prevState.product_quality
        st.capacity = st.employees
        st.roundNumber = roundNum
        st.total_customer_income = 0.0
        st.customer_price = 0.0
        st.customer_marketing = 0.0
        st.customer_quality = 0.0
        st.customer_distance = 0.0
        st.save()
    
    def startPurchaseRound(self):
        self.current_business_state.capacity = self.current_business_state.employees

    def tryPurchase(self, agent):
        self.current_business_state.attempted_purchases += 1
        if self.current_business_state.capacity > 0:
            self.current_business_state.capacity -= 1
            self.current_business_state.successful_purchases += 1
            self.current_business_state.total_customer_income += agent.income
            self.current_business_state.customer_marketing += agent.marketing_sensitivity
            self.current_business_state.customer_price += agent.price_sensitivity
            self.current_business_state.customer_distance += agent.distance_sensitivity
            self.current_business_state.customer_quality += agent.quality_sensitivity
            return True
        return False
    
    def endSimulationStep(self):
        self.current_business_state.resources += self.current_business_state.net_profit
        self.current_business_state.full_clean()
        self.current_business_state.calculate_badges()
        self.current_business_state.save()

@receiver(post_save, sender=Business, dispatch_uid = "BusinessSavedNewBusiness")
def business_saved(sender, **kwargs):
    if kwargs["created"]:
        simulation_new_business.send(sender)
