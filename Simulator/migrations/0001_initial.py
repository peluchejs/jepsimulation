# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ChanceCard'
        db.create_table('Simulator_chancecard', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('message', self.gf('django.db.models.fields.TextField')()),
            ('duration', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('Simulator', ['ChanceCard'])

        # Adding model 'FoodSettings'
        db.create_table('Simulator_foodsettings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('desire_avg', self.gf('django.db.models.fields.FloatField')(default=0.6)),
            ('desire_dev', self.gf('django.db.models.fields.FloatField')(default=0.3)),
        ))
        db.send_create_signal('Simulator', ['FoodSettings'])

        # Adding model 'MechanicSettings'
        db.create_table('Simulator_mechanicsettings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('desire_avg', self.gf('django.db.models.fields.FloatField')(default=0.45)),
            ('desire_dev', self.gf('django.db.models.fields.FloatField')(default=0.3)),
            ('desire_high_odds', self.gf('django.db.models.fields.FloatField')(default=0.2)),
            ('desire_high_avg', self.gf('django.db.models.fields.FloatField')(default=1.0)),
            ('desire_high_dev', self.gf('django.db.models.fields.FloatField')(default=0.2)),
        ))
        db.send_create_signal('Simulator', ['MechanicSettings'])

        # Adding model 'AccountingSettings'
        db.create_table('Simulator_accountingsettings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('desire_avg', self.gf('django.db.models.fields.FloatField')(default=0.35)),
            ('desire_dev', self.gf('django.db.models.fields.FloatField')(default=0.2)),
            ('desire_high_freq', self.gf('django.db.models.fields.IntegerField')(default=3)),
            ('desire_high_avg', self.gf('django.db.models.fields.FloatField')(default=0.9)),
            ('desire_high_dev', self.gf('django.db.models.fields.FloatField')(default=0.3)),
        ))
        db.send_create_signal('Simulator', ['AccountingSettings'])

        # Adding model 'PerAgentSettings'
        db.create_table('Simulator_peragentsettings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('income_max', self.gf('django.db.models.fields.FloatField')(default=7.0)),
            ('income_min', self.gf('django.db.models.fields.FloatField')(default=1.5)),
            ('income_inequality', self.gf('django.db.models.fields.FloatField')(default=0.02)),
            ('persistence_average', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('persistence_deviation', self.gf('django.db.models.fields.FloatField')(default=0.2)),
            ('price_sense_average', self.gf('django.db.models.fields.FloatField')(default=0.7)),
            ('price_sense_deviation', self.gf('django.db.models.fields.FloatField')(default=0.3)),
            ('distance_sense_average', self.gf('django.db.models.fields.FloatField')(default=0.6)),
            ('distance_sense_deviation', self.gf('django.db.models.fields.FloatField')(default=0.4)),
            ('marketing_sense_average', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('marketing_sense_deviation', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('quality_sense_average', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('quality_sense_deviation', self.gf('django.db.models.fields.FloatField')(default=0.4)),
            ('base_weights_average', self.gf('django.db.models.fields.FloatField')(default=0.6)),
            ('base_weights_deviation', self.gf('django.db.models.fields.FloatField')(default=0.2)),
            ('carryover_average', self.gf('django.db.models.fields.FloatField')(default=0.4)),
            ('carryover_deviation', self.gf('django.db.models.fields.FloatField')(default=0.3)),
            ('random_variance', self.gf('django.db.models.fields.FloatField')(default=0.05)),
        ))
        db.send_create_signal('Simulator', ['PerAgentSettings'])

        # Adding model 'CostSettings'
        db.create_table('Simulator_costsettings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('cost_per_employee', self.gf('django.db.models.fields.FloatField')(default=3.0)),
            ('base_rent_cost', self.gf('django.db.models.fields.FloatField')(default=2.0)),
            ('rent_costs', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('marketing_cost', self.gf('django.db.models.fields.FloatField')(default=2.0)),
            ('product_cost', self.gf('django.db.models.fields.FloatField')(default=1.0)),
        ))
        db.send_create_signal('Simulator', ['CostSettings'])

        # Adding model 'SimulationSettings'
        db.create_table('Simulator_simulationsettings', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('base_agent_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=7)),
            ('per_business_agent_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=10)),
            ('num_purchasing_rounds', self.gf('django.db.models.fields.PositiveIntegerField')(default=3)),
            ('starting_resources', self.gf('django.db.models.fields.FloatField')(default=25.0)),
            ('price_max', self.gf('django.db.models.fields.FloatField')(default=5.0)),
            ('price_min', self.gf('django.db.models.fields.FloatField')(default=2.0)),
            ('min_size', self.gf('django.db.models.fields.PositiveIntegerField')(default=2)),
            ('max_size', self.gf('django.db.models.fields.PositiveIntegerField')(default=9)),
            ('cost_settings', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Simulator.CostSettings'], unique=True)),
            ('food_settings', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Simulator.FoodSettings'], unique=True)),
            ('mech_settings', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Simulator.MechanicSettings'], unique=True)),
            ('acct_settings', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Simulator.AccountingSettings'], unique=True)),
            ('agent_settings', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Simulator.PerAgentSettings'], unique=True)),
        ))
        db.send_create_signal('Simulator', ['SimulationSettings'])

        # Adding model 'Simulation'
        db.create_table('Simulator_simulation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('is_current', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('settings', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['Simulator.SimulationSettings'], unique=True)),
            ('round_num', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('status', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('Simulator', ['Simulation'])

        # Adding model 'Agent'
        db.create_table('Simulator_agent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('persistence', self.gf('django.db.models.fields.FloatField')(default=0.32535136804587383)),
            ('income', self.gf('django.db.models.fields.FloatField')(default=5.64368501482659)),
            ('food_base_weight', self.gf('django.db.models.fields.FloatField')(default=0.68837658813626)),
            ('mech_base_weight', self.gf('django.db.models.fields.FloatField')(default=0.587776140017551)),
            ('acct_base_weight', self.gf('django.db.models.fields.FloatField')(default=0.88069298802981)),
            ('food_carryover', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('mech_carryover', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('acct_carryover', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('distance_sensitivity', self.gf('django.db.models.fields.FloatField')(default=0.42073984073225434)),
            ('price_sensitivity', self.gf('django.db.models.fields.FloatField')(default=0.8644347783533454)),
            ('marketing_sensitivity', self.gf('django.db.models.fields.FloatField')(default=0.351656501478556)),
            ('quality_sensitivity', self.gf('django.db.models.fields.FloatField')(default=0.5350678783831019)),
            ('carryover_percentage', self.gf('django.db.models.fields.FloatField')(default=0.6879882295149131)),
            ('resources', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('location_x', self.gf('django.db.models.fields.FloatField')(default=0.13250696873108303)),
            ('location_y', self.gf('django.db.models.fields.FloatField')(default=0.32398238392913103)),
            ('simulation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Simulator.Simulation'])),
        ))
        db.send_create_signal('Simulator', ['Agent'])

        # Adding model 'BusinessState'
        db.create_table('Simulator_businessstate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('roundNumber', self.gf('django.db.models.fields.IntegerField')(default=-1)),
            ('resources', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('attempted_purchases', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('successful_purchases', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('employees', self.gf('django.db.models.fields.IntegerField')(default=3)),
            ('marketing', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('price', self.gf('django.db.models.fields.FloatField')(default=2.0)),
            ('product_quality', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('total_customer_income', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('customer_marketing', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('customer_price', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('customer_distance', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('customer_quality', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('upgrade_costs', self.gf('django.db.models.fields.FloatField')(default=0.0)),
            ('business', self.gf('django.db.models.fields.related.ForeignKey')(related_name='states', to=orm['Simulator.Business'])),
        ))
        db.send_create_signal('Simulator', ['BusinessState'])

        # Adding model 'Business'
        db.create_table('Simulator_business', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('location_x', self.gf('django.db.models.fields.FloatField')(default=0.2715819188261908)),
            ('location_y', self.gf('django.db.models.fields.FloatField')(default=0.7226455907627933)),
            ('area', self.gf('django.db.models.fields.IntegerField')(default=2)),
            ('size', self.gf('django.db.models.fields.IntegerField')(default=6)),
            ('color', self.gf('django.db.models.fields.IntegerField')(null=True)),
            ('can_be_modified', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('simulation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Simulator.Simulation'])),
        ))
        db.send_create_signal('Simulator', ['Business'])


    def backwards(self, orm):
        # Deleting model 'ChanceCard'
        db.delete_table('Simulator_chancecard')

        # Deleting model 'FoodSettings'
        db.delete_table('Simulator_foodsettings')

        # Deleting model 'MechanicSettings'
        db.delete_table('Simulator_mechanicsettings')

        # Deleting model 'AccountingSettings'
        db.delete_table('Simulator_accountingsettings')

        # Deleting model 'PerAgentSettings'
        db.delete_table('Simulator_peragentsettings')

        # Deleting model 'CostSettings'
        db.delete_table('Simulator_costsettings')

        # Deleting model 'SimulationSettings'
        db.delete_table('Simulator_simulationsettings')

        # Deleting model 'Simulation'
        db.delete_table('Simulator_simulation')

        # Deleting model 'Agent'
        db.delete_table('Simulator_agent')

        # Deleting model 'BusinessState'
        db.delete_table('Simulator_businessstate')

        # Deleting model 'Business'
        db.delete_table('Simulator_business')


    models = {
        'Simulator.accountingsettings': {
            'Meta': {'object_name': 'AccountingSettings'},
            'desire_avg': ('django.db.models.fields.FloatField', [], {'default': '0.35'}),
            'desire_dev': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'desire_high_avg': ('django.db.models.fields.FloatField', [], {'default': '0.9'}),
            'desire_high_dev': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'desire_high_freq': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Simulator.agent': {
            'Meta': {'object_name': 'Agent'},
            'acct_base_weight': ('django.db.models.fields.FloatField', [], {'default': '0.8218078244980351'}),
            'acct_carryover': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'carryover_percentage': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'distance_sensitivity': ('django.db.models.fields.FloatField', [], {'default': '0.5909332270978461'}),
            'food_base_weight': ('django.db.models.fields.FloatField', [], {'default': '0.7063668565841347'}),
            'food_carryover': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'income': ('django.db.models.fields.FloatField', [], {'default': '5.272597972280167'}),
            'location_x': ('django.db.models.fields.FloatField', [], {'default': '0.27190922693361264'}),
            'location_y': ('django.db.models.fields.FloatField', [], {'default': '0.8585617768136871'}),
            'marketing_sensitivity': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'mech_base_weight': ('django.db.models.fields.FloatField', [], {'default': '0.30651819918841483'}),
            'mech_carryover': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'persistence': ('django.db.models.fields.FloatField', [], {'default': '0.28700848319317'}),
            'price_sensitivity': ('django.db.models.fields.FloatField', [], {'default': '0.2616847107933851'}),
            'quality_sensitivity': ('django.db.models.fields.FloatField', [], {'default': '0.42485824591205934'}),
            'resources': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'simulation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Simulator.Simulation']"})
        },
        'Simulator.business': {
            'Meta': {'object_name': 'Business'},
            'area': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'can_be_modified': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location_x': ('django.db.models.fields.FloatField', [], {'default': '0.4537197521490679'}),
            'location_y': ('django.db.models.fields.FloatField', [], {'default': '0.45342843840661234'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'simulation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Simulator.Simulation']"}),
            'size': ('django.db.models.fields.IntegerField', [], {'default': '6'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'Simulator.businessstate': {
            'Meta': {'object_name': 'BusinessState'},
            'attempted_purchases': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'business': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'states'", 'to': "orm['Simulator.Business']"}),
            'customer_distance': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'customer_marketing': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'customer_price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'customer_quality': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'employees': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marketing': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'product_quality': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'resources': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'roundNumber': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'successful_purchases': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'total_customer_income': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'upgrade_costs': ('django.db.models.fields.FloatField', [], {'default': '0.0'})
        },
        'Simulator.chancecard': {
            'Meta': {'object_name': 'ChanceCard'},
            'duration': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {})
        },
        'Simulator.costsettings': {
            'Meta': {'object_name': 'CostSettings'},
            'base_rent_cost': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'cost_per_employee': ('django.db.models.fields.FloatField', [], {'default': '3.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marketing_cost': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'product_cost': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'rent_costs': ('django.db.models.fields.FloatField', [], {'default': '0.5'})
        },
        'Simulator.foodsettings': {
            'Meta': {'object_name': 'FoodSettings'},
            'desire_avg': ('django.db.models.fields.FloatField', [], {'default': '0.6'}),
            'desire_dev': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Simulator.mechanicsettings': {
            'Meta': {'object_name': 'MechanicSettings'},
            'desire_avg': ('django.db.models.fields.FloatField', [], {'default': '0.45'}),
            'desire_dev': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'desire_high_avg': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'desire_high_dev': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'desire_high_odds': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Simulator.peragentsettings': {
            'Meta': {'object_name': 'PerAgentSettings'},
            'base_weights_average': ('django.db.models.fields.FloatField', [], {'default': '0.6'}),
            'base_weights_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'carryover_average': ('django.db.models.fields.FloatField', [], {'default': '0.4'}),
            'carryover_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'distance_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.6'}),
            'distance_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'income_inequality': ('django.db.models.fields.FloatField', [], {'default': '0.02'}),
            'income_max': ('django.db.models.fields.FloatField', [], {'default': '7.0'}),
            'income_min': ('django.db.models.fields.FloatField', [], {'default': '1.5'}),
            'marketing_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'marketing_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'persistence_average': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'persistence_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'price_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.7'}),
            'price_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'quality_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'quality_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.4'}),
            'random_variance': ('django.db.models.fields.FloatField', [], {'default': '0.05'})
        },
        'Simulator.simulation': {
            'Meta': {'object_name': 'Simulation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_current': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'round_num': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.SimulationSettings']", 'unique': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'Simulator.simulationsettings': {
            'Meta': {'object_name': 'SimulationSettings'},
            'acct_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.AccountingSettings']", 'unique': 'True'}),
            'agent_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.PerAgentSettings']", 'unique': 'True'}),
            'base_agent_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '7'}),
            'cost_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.CostSettings']", 'unique': 'True'}),
            'food_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.FoodSettings']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_size': ('django.db.models.fields.PositiveIntegerField', [], {'default': '9'}),
            'mech_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.MechanicSettings']", 'unique': 'True'}),
            'min_size': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2'}),
            'num_purchasing_rounds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'per_business_agent_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10'}),
            'price_max': ('django.db.models.fields.FloatField', [], {'default': '5.0'}),
            'price_min': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'starting_resources': ('django.db.models.fields.FloatField', [], {'default': '25.0'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Simulator']