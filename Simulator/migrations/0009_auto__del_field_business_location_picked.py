# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Business.location_picked'
        db.delete_column('Simulator_business', 'location_picked')


    def backwards(self, orm):
        # Adding field 'Business.location_picked'
        db.add_column('Simulator_business', 'location_picked',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    models = {
        'Simulator.accountingsettings': {
            'Meta': {'object_name': 'AccountingSettings'},
            'desire_avg': ('django.db.models.fields.FloatField', [], {'default': '0.35'}),
            'desire_dev': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'desire_high_avg': ('django.db.models.fields.FloatField', [], {'default': '0.9'}),
            'desire_high_dev': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'desire_high_freq': ('django.db.models.fields.IntegerField', [], {'default': '3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Simulator.agent': {
            'Meta': {'object_name': 'Agent'},
            'acct_base_weight': ('django.db.models.fields.FloatField', [], {}),
            'acct_carryover': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'carryover_percentage': ('django.db.models.fields.FloatField', [], {}),
            'distance_sensitivity': ('django.db.models.fields.FloatField', [], {}),
            'food_base_weight': ('django.db.models.fields.FloatField', [], {}),
            'food_carryover': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'income': ('django.db.models.fields.FloatField', [], {}),
            'location_x': ('django.db.models.fields.FloatField', [], {'default': '0.6636066617212104'}),
            'location_y': ('django.db.models.fields.FloatField', [], {'default': '0.7887867046531167'}),
            'marketing_sensitivity': ('django.db.models.fields.FloatField', [], {}),
            'mech_base_weight': ('django.db.models.fields.FloatField', [], {}),
            'mech_carryover': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'persistence': ('django.db.models.fields.FloatField', [], {}),
            'price_sensitivity': ('django.db.models.fields.FloatField', [], {}),
            'quality_sensitivity': ('django.db.models.fields.FloatField', [], {}),
            'resources': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'simulation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Simulator.Simulation']"})
        },
        'Simulator.badge': {
            'Meta': {'object_name': 'Badge'},
            'business_state': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Simulator.BusinessState']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        'Simulator.business': {
            'Meta': {'object_name': 'Business'},
            'area': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'color': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location_x': ('django.db.models.fields.FloatField', [], {'default': '0.5617625967947946'}),
            'location_y': ('django.db.models.fields.FloatField', [], {'default': '0.680664245396484'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'simulation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Simulator.Simulation']"}),
            'size': ('django.db.models.fields.IntegerField', [], {'default': '6'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'Simulator.businessstate': {
            'Meta': {'object_name': 'BusinessState'},
            'attempted_purchases': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'business': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'states'", 'to': "orm['Simulator.Business']"}),
            'customer_distance': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'customer_marketing': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'customer_price': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'customer_quality': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'employees': ('django.db.models.fields.IntegerField', [], {'default': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marketing': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'price': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'product_quality': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'resources': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'roundNumber': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'successful_purchases': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'total_customer_income': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'upgrade_costs': ('django.db.models.fields.FloatField', [], {'default': '0.0'})
        },
        'Simulator.costsettings': {
            'Meta': {'object_name': 'CostSettings'},
            'base_rent_cost': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'cost_per_employee': ('django.db.models.fields.FloatField', [], {'default': '3.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'marketing_cost': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'product_cost': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'rent_costs': ('django.db.models.fields.FloatField', [], {'default': '0.5'})
        },
        'Simulator.foodsettings': {
            'Meta': {'object_name': 'FoodSettings'},
            'desire_avg': ('django.db.models.fields.FloatField', [], {'default': '0.6'}),
            'desire_dev': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Simulator.mechanicsettings': {
            'Meta': {'object_name': 'MechanicSettings'},
            'desire_avg': ('django.db.models.fields.FloatField', [], {'default': '0.45'}),
            'desire_dev': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'desire_high_avg': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'desire_high_dev': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'desire_high_odds': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Simulator.peragentsettings': {
            'Meta': {'object_name': 'PerAgentSettings'},
            'base_weights_average': ('django.db.models.fields.FloatField', [], {'default': '0.6'}),
            'base_weights_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'carryover_average': ('django.db.models.fields.FloatField', [], {'default': '0.4'}),
            'carryover_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'distance_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.6'}),
            'distance_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.4'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'income_inequality': ('django.db.models.fields.FloatField', [], {'default': '0.02'}),
            'income_max': ('django.db.models.fields.FloatField', [], {'default': '7.0'}),
            'income_min': ('django.db.models.fields.FloatField', [], {'default': '1.5'}),
            'marketing_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'marketing_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'persistence_average': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'persistence_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.2'}),
            'price_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.7'}),
            'price_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.3'}),
            'quality_sense_average': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            'quality_sense_deviation': ('django.db.models.fields.FloatField', [], {'default': '0.4'}),
            'random_variance': ('django.db.models.fields.FloatField', [], {'default': '0.05'})
        },
        'Simulator.simulation': {
            'Meta': {'object_name': 'Simulation'},
            'acct_desire_pred': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'food_desire_pred': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'mech_desire_pred': ('django.db.models.fields.FloatField', [], {'default': '0.0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'projector_business': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'null': 'True', 'to': "orm['Simulator.Business']"}),
            'projector_state': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'round_num': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.SimulationSettings']", 'unique': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'Simulator.simulationsettings': {
            'Meta': {'object_name': 'SimulationSettings'},
            'acct_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.AccountingSettings']", 'unique': 'True'}),
            'agent_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.PerAgentSettings']", 'unique': 'True'}),
            'base_agent_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '7'}),
            'cost_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.CostSettings']", 'unique': 'True'}),
            'food_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.FoodSettings']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_size': ('django.db.models.fields.PositiveIntegerField', [], {'default': '9'}),
            'mech_settings': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['Simulator.MechanicSettings']", 'unique': 'True'}),
            'min_size': ('django.db.models.fields.PositiveIntegerField', [], {'default': '2'}),
            'num_purchasing_rounds': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'per_business_agent_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10'}),
            'price_max': ('django.db.models.fields.FloatField', [], {'default': '5.0'}),
            'price_min': ('django.db.models.fields.FloatField', [], {'default': '2.0'}),
            'starting_resources': ('django.db.models.fields.FloatField', [], {'default': '150.0'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Simulator']