class SimulationStateException(BaseException):
    def __init__(self, msg):
        self.message = msg

class ProjectorOperationException(BaseException):
    def __init__(self, msg):
        self.message = msg