$(document).ready(function() {
	$("input.slider").each(function() {
		var sliderDiv = $("<div class='actualSlider'></div>");
		var showText = $(this).attr("data-showText");
		if (showText == undefined) {
			showText = true;
		} else {
			showText = (showText == "true");
		}
		
		$(this).after(sliderDiv);
		
		if (showText) {
			$(this).attr("readonly", "true");
		} else {
			var lowLabel = $(this).attr("data-lowLabel");
			var highLabel = $(this).attr("data-highLabel");
			if (lowLabel && highLabel) {
				$(this).after("<div class='sliderLabels'><span class='lowLabel'>" + lowLabel + "</span><span class='highLabel'>" + highLabel + "</span></div>");	
			}
			
			$(this).hide();
		}
		var textEle = this;
		
		$(sliderDiv[0]).slider({
			value: parseFloat(this.value),
			min: parseFloat($(this).attr("data-min")),
			max: parseFloat($(this).attr("data-max")),
			step: 0.1,
			slide: function(event, ui) {
				textEle.value = ui.value;
				updateCosts();	
			}
		});
	});
	
	$("input.snapSlider").each(function() {
		var sliderDiv = $("<div class='actualSlider'></div>");
		var showText = $(this).attr("data-showText");
		if (showText == undefined) {
			showText = true;
		}
		
		$(this).after(sliderDiv);
		
		if (showText) {
			$(this).attr("readonly", "true");
		} else {
			$(this).hide();
		}
		
		var textEle = this;
		
		$(sliderDiv[0]).slider({
			value: parseFloat(this.value),
			min: parseFloat($(this).attr("data-min")),
			max: parseFloat($(this).attr("data-max")),
			slide: function(event, ui) {
				textEle.value = ui.value;
				updateCosts();		
			}
		});
	});
	
	$("input.spinner").each(function() {
		var min = parseInt($(this).attr("data-min"), 10);
		var max = parseInt($(this).attr("data-max"), 10);
		$(this).spinner({
			max: max,
			min: min,
			spin: function(event, ui) {
				updateCosts();
			},
			change: function(event, ui) {
				updateCosts();
			}
		});
	});
	
	$("input.locationPicker").each(function() {
		var $this = $(this);
		var locationDiv = $("<div class='locPicker'></div>");
		$this.after(locationDiv);
		$this.hide();
		$(locationDiv[0]).locationPicker({
			selectionChanged: function(event, data) {
				$this[0].value = data.newIndex;
			}
		});
	});
	
	updateCosts();
});