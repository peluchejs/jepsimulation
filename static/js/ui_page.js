$(document).ready(function() {
	$( "#tabs" ).tabs({
			beforeLoad: function( event, ui ) {
				ui.jqXHR.error(function() {
					ui.panel.html(
						"<img src='/static/img/load_fail.png' alt='Uh-oh!' class='failImage'></img>"+
						"<p class='failText'>He's dead, Jim!<br>" +
						"Sorry about this, but it looks like we're having some problems. " +
						"We'll try to have things working again as soon as we can!<br>" +
						"In the meantime, make sure you have a good network connection.</p>");
				});
			},
			activate: function(event, ui) {
				$(ui.newPanel).find(".chart").each(function() {
					refreshChart(this.id);
				});
				
				// TODO: How to edit the url without making the page jump downward?
				// window.location = ui.newTab.context.href;
			},
			load: function(event, ui) {
				$(ui.panel).find("input[type=submit]").button();
				$(ui.panel).find("form").submit(function(event) {
					var $form = $(this);
					var thedata = $form.serialize();
					$.post(this.action, thedata).success(function(data, textStatus, jqXHR){
						
					}).fail(function() {
						$form.submit();
					});
					event.preventDefault();
				});
			}
		});
	$(".accordion").accordion();
});
