var failCount = 0;

function sendComet(destination, successCallback)
{
	$.ajax(destination).done(function() {
		failCount = 0;
		successCallback();
	}).fail(function() {
		failCount++;
		if (failCount < 5) {
			sendComet(destination, successCallback);
		}
	});
}
