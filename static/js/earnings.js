$(function() {
	var minValue = -25.0;
	var maxValue = 25.0;
	
    // the widget definition, where "custom" is the namespace,
    // "colorize" the widget name
    $.widget( "jefazo.earnings", {
      // default options
      options: {
 		value: 0.0
      },
 
      // the constructor
      _create: function() {
        this.element
          .addClass( "jefazo-earnings ui-widget ui-widget-content ui-corner-all" )
          .disableSelection();
 
        this.indicator = $("<div class='jefazo-indicator ui-state-default ui-corner-all'></div>")
        	.appendTo( this.element );
 
        this._refresh();
      },
 
      // called when created, and later when changing options
      _refresh: function() {
      	var clampedValue = Math.min(maxValue, Math.max(minValue, this.options.value));
      	var percentage = 100.0 * (clampedValue - minValue) / (maxValue - minValue);
        this.indicator.css("left", percentage + "%");
      },
 
      // events bound via _on are removed automatically
      // revert other modifications here
      _destroy: function() {
        // remove generated elements
        this.indicator.remove();
 
        this.element
          .removeClass( "jefazo-earnings ui-widget ui-widget-content ui-corner-all" )
          .enableSelection();
      },
 
      // _setOptions is called with a hash of all options that are changing
      // always refresh when changing options
      _setOptions: function() {
        // _super and _superApply handle keeping the right this-context
        this._superApply( arguments );
        this._refresh();
      },
 
      // _setOption is called for each individual option that is changing
      _setOption: function( key, value ) {
        this._super( key, value );
      }
    });
 });