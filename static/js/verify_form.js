function textReady()
{
	allHaveValues = true;
	$("input[type=text]").each(function() {
		allHaveValues = allHaveValues && (this.value !== "");
	});
	
	return allHaveValues;
}

function passwordReady()
{
	pass1 = $("#id_password1").get(0).value;
	pass2 = $("#id_password2").get(0).value;
	if (pass1 != pass2 || pass1 === "")
	{
		return false;
	}
	
	return true;
}

// Only enable submit button once all elements are filled out
function inputChanged()
{
	if (textReady() && passwordReady())
	{
		$("#id_submit").removeAttr('disabled');
	}
}

$(document).ready(function() {
	$("input[type=password]").bind("change keyup", inputChanged);
	$("input[type=text]").change(inputChanged);
	$("#id_submit").attr('disabled', 'disabled');
});
