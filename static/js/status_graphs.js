var charts = {}

function refreshChart(chartDivId) {
	if (charts[chartDivId]) {
		charts[chartDivId].replot();
	}
}

function getHeaders(chartDivId)
{
	var headers = [];
	$("#" + chartDivId + " table thead tr th").each(function() {
		headers.push(this.innerText);
	});
	
	return headers;	
}

function getData(chartDivId)
{
	var data = [];

	$("#" + chartDivId + " table tbody tr").each(function() {
		data.push([]);
		$(this).find("td").each(function() {
			data[data.length-1].push(this.innerText);
		});
	});
	
	$("#" + chartDivId + " table").hide();
	
	return data;
}

function makePieChart(chartDivId, data, chartTitle)
{
	var chartData = data.map(function(ele) { return [ele[0], parseFloat(ele[1])]; });
	
	charts[chartDivId] =  $.jqplot(chartDivId, [chartData], {
					title: chartTitle,
      				seriesDefaults: {
        				// Make this a pie chart.
        				renderer: jQuery.jqplot.PieRenderer, 
        				rendererOptions: {
          					// Put data labels on the pie slices.
          					// By default, labels show the percentage of the slice.
          					showDataLabels: true
        				}
      				}, 
      				legend: { show:true, location: 'e' }
    			});
}

function makeBarChart(chartDivId, data)
{
	var seriesNames = data.map(function(ele){ return ele[0]; });
	var chartData = [];
	for(i = 1; i < data[0].length; i++)
	{ 
		chartData.push(data.map(function(ele){ return parseFloat(ele[i]); }))
	}

    charts[chartDivId] =  $.jqplot(chartDivId, chartData, {
                 seriesDefaults:{
                     renderer: $.jqplot.BarRenderer,
                     rendererOptions: {
                        barPadding: 1,
                        barMargin: 15,
                        barDirection: 'vertical',
                        barWidth: 30 / chartData.length,
                        fillToZero: true
                    }, 
                    pointLabels: { show: true }
                },
                axes: {
                    xaxis: {                            
                            renderer:  $.jqplot.CategoryAxisRenderer,
                            ticks: seriesNames
                    },
                    yaxis: {
                        tickOptions: {
                            formatString: '%.2f',
                            pad: 1.05
                        }
                    }
                },
                highlighter: {
                    sizeAdjust: 7.5
                },
                cursor: {
                    show: true
                }
            });
}

function makeLineChart(chartDivId, headers, data)
{
	var seriesNames = data.map(function(ele){ return ele.shift(); });
	var chartData = Array();
	var seriesInfo = [];
	var i;
	
	for(i = 0; i < data[0].length; i++) {
		var currSeries = data.map(function(e) {
			return parseFloat(e[i]);
		});
		
		chartData.push(currSeries);
	}
	
	for (i = 0; i < chartData.length; i++) {
		seriesInfo.push({label: headers[i + 1], yaxis: i % 2 ? "yaxis" : "y2axis"});
	}
	
	charts[chartDivId] =  $.jqplot(chartDivId, chartData, {
		series: seriesInfo,
		axes: {
			xaxis: {                            
                renderer:  $.jqplot.CategoryAxisRenderer,
                ticks: seriesNames
           },
           yaxis: {
           	labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
            labelOptions:{
                fontFamily:'Helvetica',
                fontSize: '14pt'
            },
            label:seriesInfo[1].label
           },
           y2axis: {
           	labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
            labelOptions:{
                fontFamily:'Helvetica',
                fontSize: '14pt'
            },
            label:seriesInfo[0].label
           }
		},
		legend: {
      		show: true,
      		location: 'se',
      		placement: 'inside'
    	}
	});
}

function makeStackedBarChart(chartDivId, headers, data)
{
	var seriesNames = data.map(function(ele){ return ele[0]; });
	var chartData = data.map(function(ele){ ele.shift(); return ele.map(function(se){ return parseFloat(se); }); });
	var reorderedData = Array(chartData[0].length);
	var i;
	for (i = 0; i < chartData[0].length; i++) {
		reorderedData[i] = Array(chartData.length);
	}
	
	for (i = 0; i < chartData.length; i++) {
		ele = chartData[i];
		for (j = 0; j < ele.length; j++)
		{
			reorderedData[j][i] = chartData[i][j];
		}
	}
	
	var seriesInfo = []
	for (i = 1; i < headers.length; i++) {
		seriesInfo.push({ label: headers[i]} );
	}

    charts[chartDivId] = $.jqplot(chartDivId, reorderedData, {
    	// Tell the plot to stack the bars.
    	stackSeries: true,
    	captureRightClick: true,
    	seriesDefaults:{
      	renderer:$.jqplot.BarRenderer,
      	rendererOptions: {
          	// Put a 30 pixel margin between bars.
          	barMargin: 30
      	},
      	pointLabels: {show: true}
    	},
    	series: seriesInfo,
    	axes: {
      	xaxis: {
          	renderer: $.jqplot.CategoryAxisRenderer,
          	ticks: seriesNames
      	},
      	yaxis: {
	        // Don't pad out the bottom of the data range.  By default,
        	// axes scaled as if data extended 10% above and below the
        	// actual range to prevent data points right on grid boundaries.
        	// Don't want to do that here.
        	padMin: 0
      	}
    	},
    	legend: {
      	show: true,
      	location: 'e',
      	placement: 'inside'
    	}      
  	});
}

$(document).ready(function () {
	$.jqplot.config.enablePlugins = true;
	
	$("div.barchart").each(function() {
		makeBarChart(this.id, getData(this.id));
	});
	
	$("div.piechart").each(function() {
		data = getData(this.id);
		titleElement = $(this).children("header").get(0);
		title = null;
		if (titleElement)
		{
			title = titleElement.innerText;
		}
		$(this).empty();
		makePieChart(this.id, data, title);
	});
	
	$("div.linechart").each(function() {
		headers = getHeaders(this.id);
		data = getData(this.id);
		makeLineChart(this.id, headers, data);
	});
	
	$("div.stackedbarchart").each(function() {
		headers = getHeaders(this.id);
		data = getData(this.id);
		makeStackedBarChart(this.id, headers, data);
	});
});
