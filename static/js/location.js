(function($) {
	
    // the widget definition, where "custom" is the namespace,
    // "colorize" the widget name
    $.widget( "jefazo.locationPicker", {
      // default options
      options: {
 		neighborhoodInfo: [
 			{
 				name: "Fremont",
 				population: "lowpop.png",
 				income: "rich.png"
 			},
 			{
 				name: "Montlake",
 				population: "medpop.png",
 				income: "middle.png"
 			},
 			{
 				name: "Madison",
 				population: "medpop.png",
 				income: "middle.png"
 			},
 			{
 				name: "Sodo",
 				population: "highpop.png",
 				income: "poor.png"
 			}
 		],
 		
 		dataUrl: "/simulator/mapdata/",
 		cometUrl: "/comet/refreshMap",
 		autoRefresh: true,
 		selected: -1,
 		selectionChanged: null
      },
 
      // the constructor
      _create: function() {
        this.element
          // add a class for theming
          .addClass( "jefazo-location-picker ui-widget" )
          // prevent double click to select text
          .disableSelection();
 
        //this.indicator = $("<div class='jefazo-indicator ui-state-default ui-corner-all'></div>")
        //	.appendTo( this.element );
 		this.neighborhoods = Array();
 		this.cometLaunched = false;
 		this.dataLoaded = false;
 		
 		var locations = Array("jefazo-location-nw",
 							  "jefazo-location-ne",
 							  "jefazo-location-sw",
 							  "jefazo-location-se");

 		for (var i = 0; i < 4; i++) {
 			this.neighborhoods.push(
 				$("<div class='jefazo-location " + locations[i] + " ui-widget ui-widget-content ui-corner-all' data-index='" + i + "'></div>")
 					.appendTo( this.element));
 			this.neighborhoods[i].append("<div class='labelDiv'><h2>" + this.options.neighborhoodInfo[i].name + "</h2>" +
 			    "<div class='dataDiv'>" +
 			    "<div class='popDiv' style='background-image:url(/static/img/" + this.options.neighborhoodInfo[i].population + ");'></div>" +
				"<div class='moneyDiv' style='background-image:url(/static/img/" + this.options.neighborhoodInfo[i].income + ")'></div>" +
				"<div class='motoDiv'><span class='motoData dataSpan'>0</span></div>" +
				"<div class='foodDiv'><span class='foodData dataSpan'>0</span></div>" +
				"<div class='acctDiv'><span class='acctData dataSpan'>0</span></div>" +
 			    "</div>" + 
 				"</div>");
 				
 			this._on(this.neighborhoods[i], {
 				click: "selectNeighborhood"
 			});
 		}
        this._refresh();
      },
      
      setData: function(data) {
			// set the data
			this.dataLoaded = true;
			for (var i = 0; i < 4; i++) {
				this.neighborhoods[i].find(".motoData")[0].innerText = (data[i].moto);
				this.neighborhoods[i].find(".foodData")[0].innerText = (data[i].food);
				this.neighborhoods[i].find(".acctData")[0].innerText = (data[i].acct);
			}
      },
 
      // called when created, and later when changing options
      _refresh: function() {
      		var myThis = this;
			if (this.options.autoRefresh && !this.cometLaunched) {
				if (!this.dataLoaded) {
					$.getJSON(this.options.dataUrl)
						.success(function(data) {
							myThis.setData(data);
						});
				}
				
				sendComet(this.options.cometUrl, function() {
					$.getJSON(myThis.options.dataUrl)
						.success(function(data) {
							myThis.setData(data);
						});
				});
				
				this.cometLaunched = true;
			}
      },
 
      // events bound via _on are removed automatically
      // revert other modifications here
      _destroy: function() {
        // remove generated elements
        for (var i = 0; i < this.neighborhoods.length; i++) {
        	this.neighborhoods[i].remove();
        }
 
        this.element
          .removeClass( "jefazo-location-picker ui-widget ui-widget-content ui-corner-all" )
          .enableSelection();
      },
 
      // _setOptions is called with a hash of all options that are changing
      // always refresh when changing options
      _setOptions: function() {
        // _super and _superApply handle keeping the right this-context
        this._superApply( arguments );
        this._refresh();
      },
 
      // _setOption is called for each individual option that is changing
      _setOption: function( key, value ) {
        this._super( key, value );
      },
      
      selectNeighborhood: function(event) {
      	var $target = $(event.currentTarget);
      	if ($target.filter(".jefazo-location").length) {
      		var index = parseInt($($target.get(0)).attr("data-index"), 10);
      		if ((index || index == 0) && index != -1 && index != this.options.selected) {
      			if (this.options.selected != -1) {
      				this.neighborhoods[this.options.selected].removeClass("jefazo-location-selected ui-state-highlight");
      			}
      			
      			var oldIndex = this.options.selected;
      			var newIndex = index;
      			$target.addClass("jefazo-location-selected ui-state-highlight");
      			this.options.selected = index;
      			this._trigger("selectionChanged", event, {oldIndex: oldIndex, newIndex: newIndex});
      		}
      	}
      }
    });
 })($);