function Timer(interval, callback) {
	this.interval = interval;
	this.callback = callback;
	this.display = $("#timeLeft").get(0);
}

Timer.prototype.run = function() {
	var t = this;
	this.timerId = setInterval(function() {
		t.interval -= 1;
		t.displayTime();
		if (t.interval == 0) {
			t.cancel();
			t.callback();
		}
	}, 1000);
	
	this.displayTime();
}

Timer.prototype.cancel = function() {
	clearInterval(this.timerId);
	this.display.innerText = "-:--";
}

Timer.prototype.displayTime = function() {
	var minutes = Math.floor(this.interval / 60);
	var seconds = this.interval - (minutes * 60);
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	var timeStr = minutes + ":" + seconds;
	this.display.innerText = timeStr;
}

$(document).ready(function() {
	var t;
	const PROJECTOR_URL = "/simulator/admin/project/";
	
	var csrf_element = $("#projectorDiv :hidden").get(0).firstChild;
	const CSRF_TOKEN = csrf_element.value;
	$("#stopButton").hide();
	$(".radioset").buttonset();
	$(".spinner").spinner();
	$("button").button();
	
	$("#showGreeting").button().click(function() {
		$.post(PROJECTOR_URL, {
			"mode": "greeting",
			"csrfmiddlewaretoken": CSRF_TOKEN
		});
	});
	
	$("#showTimer").button().click(function(){
		var timeRequested = parseInt($("#secondsSpinner").get(0).value);
		$.post(PROJECTOR_URL, {
			"mode": "timer",
			"seconds": timeRequested,
			"csrfmiddlewaretoken": CSRF_TOKEN
		});
	});
	
	$("#showUser").button().click(function() {
		var userRequested = $("#userSelect").get(0).value;
		if (userRequested == "") {
			return;
		}
		
		$.post(PROJECTOR_URL, {
			"mode": "user",
			"business_id": userRequested,
			"csrfmiddlewaretoken": CSRF_TOKEN
		});
	});
	
	$("#showBoard").button().click(function() {
		$.post(PROJECTOR_URL, {
			"mode": "board",
			"csrfmiddlewaretoken": CSRF_TOKEN
		});
	});
	
	function runWaitTimer() {
		t = new Timer(parseInt($("#waitTimeText").get(0).value), function() {
			$("#statusSpan").get(0).innerText = "Allowing modifications ";
			$.ajax("/simulator/admin/beginstep/").success(function() {
				$("#statusSpan").get(0).innerText = "Success. Running step in ";
				runStepTimer();
			}).fail(function() {
				$("#statusSpan").get(0).innerText = "Failed!";
			});
		});
		t.run();
	}
	
	function runStepTimer() {
		t = new Timer(parseInt($("#modifyTimeText").get(0).value), function() {
			$("#statusSpan").get(0).innerText = "Running next step ";
			$.ajax("/simulator/admin/runstep/").success(function() {
				$("#statusSpan").get(0).innerText = "Success. Allowing modifications in ";
				runWaitTimer();
			}).fail(function() {
				$("#statusSpan").get(0).innerText = "Failed!";
			});
		});
		t.run();
	}
	
	$("#startButton").button().click(function(event){
		$("#stopButton").show();
		$("#startButton").hide();
		runWaitTimer();
		event.preventDefault();
	});
	
	$("#stopButton").button().click(function(event){
		$("#startButton").show();
		$("#stopButton").hide();
		if (t) {
			t.cancel();
		}
		event.preventDefault();
	});
	
	$("a.ajaxbutton").button().click(function(event){
		$.ajax(event.currentTarget.href).success(function() {
			
		}).fail(function() {
			
		});
		event.preventDefault();
	});
});
