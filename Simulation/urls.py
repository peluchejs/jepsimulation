from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^$', 'Simulation.views.chooserole'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^simulator/', include('Simulator.urls')),
    url(r'^user/', include('UserManagement.urls')),
)
