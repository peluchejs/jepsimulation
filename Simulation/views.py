import Simulator.views
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

@login_required
def chooserole(request):
    return HttpResponseRedirect(reverse(Simulator.views.choose_simulation))
