from threading import Timer
from datetime import datetime
from math import floor

class ServerTimer(object):
    running = False
    seconds = 0
    timer = None
    
    @classmethod
    def start(cls, seconds, expired):
        if cls.running:
            cls.stop()
        cls.seconds = seconds
        cls.starttime =  datetime.utcnow()
        cls.timer = Timer(seconds, expired)
        cls.timer.start()
        cls.running = True
    
    @classmethod
    def stop(cls):
        if cls.timer is not None:
            cls.timer.cancel()
        cls.timer = None
        cls.running = False
    
    @classmethod
    def remaining(cls):
        if cls.running:
            end = datetime.utcnow()
            diff = end - cls.starttime
            remains = cls.seconds - floor(diff.total_seconds())
            return remains if remains > 0 else 0 
        return 0